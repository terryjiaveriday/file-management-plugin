import React from 'react';
import { render, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import dWFileMocks from 'components/__mocks__/dWFileMocks';
import { apiDWFilesGet } from 'api/dWFiles';
import 'i18n/__mocks__/i18nMock';
import DWFileTableContainer from 'components/DWFileTableContainer';

jest.mock('api/dWFiles');

jest.mock('auth/withKeycloak', () => {
  const withKeycloak = (Component) => {
    return (props) => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        keycloak={{
          initialized: true,
          authenticated: true,
        }}
      />
    );
  };

  return withKeycloak;
});

jest.mock('components/pagination/withPagination', () => {
  const withPagination = (Component) => {
    return (props) => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        pagination={{
          onChangeItemsPerPage: () => {},
          onChangeCurrentPage: () => {},
        }}
      />
    );
  };

  return withPagination;
});

describe('DWFileTableContainer', () => {
  const errorMessageKey = 'error.dataLoading';

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('calls API', async () => {
    apiDWFilesGet.mockImplementation(() => Promise.resolve({ dWFiles: dWFileMocks, count: 2 }));
    const { queryByText } = render(<DWFileTableContainer />);

    await wait(() => {
      expect(apiDWFilesGet).toHaveBeenCalledTimes(1);
      expect(queryByText(errorMessageKey)).not.toBeInTheDocument();
    });
  });

  it('shows an error if the API call is not successful', async () => {
    apiDWFilesGet.mockImplementation(() => {
      throw new Error();
    });
    const { getByText } = render(<DWFileTableContainer />);

    wait(() => {
      expect(apiDWFilesGet).toHaveBeenCalledTimes(1);
      expect(getByText(errorMessageKey)).toBeInTheDocument();
    });
  });
});
