package com.veriday.filemanagement.repository;

public interface DWEmailRepository {

    void sendMessageSimple(String userId, String to, String from, String subject, String bodyTest) throws Exception;

}
