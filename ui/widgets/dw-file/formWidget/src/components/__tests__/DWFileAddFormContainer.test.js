import React from 'react';
import { fireEvent, render, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { apiDWFilePost } from 'api/dWFiles';
import DWFileAddFormContainer from 'components/DWFileAddFormContainer';
import 'i18n/__mocks__/i18nMock';
import dWFileMock from 'components/__mocks__/dWFileMocks';

jest.mock('api/dWFiles');
jest.mock('@material-ui/pickers', () => {
  // eslint-disable-next-line react/prop-types
  const MockPicker = ({ id, value, name, label, onChange }) => {
    const handleChange = (event) => onChange(event.currentTarget.value);
    return (
      <span>
        <label htmlFor={id}>{label}</label>
        <input id={id} name={name} value={value || ''} onChange={handleChange} />
      </span>
    );
  };
  return {
    ...jest.requireActual('@material-ui/pickers'),
    DateTimePicker: MockPicker,
    DatePicker: MockPicker,
  };
});

jest.mock('auth/withKeycloak', () => {
  const withKeycloak = (Component) => {
    return (props) => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        keycloak={{
          initialized: true,
          authenticated: true,
        }}
      />
    );
  };

  return withKeycloak;
});

describe('DWFileAddFormContainer', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  const errorMessageKey = 'error.dataLoading';
  const successMessageKey = 'common.dataSaved';

  const onErrorMock = jest.fn();
  const onCreateMock = jest.fn();

  it('saves data', async () => {
    apiDWFilePost.mockImplementation((data) => Promise.resolve(data));

    const { findByTestId, findByLabelText, queryByText, rerender } = render(
      <DWFileAddFormContainer onError={onErrorMock} onUpdate={onCreateMock} />
    );

    const fileIdField = await findByLabelText('entities.dWFile.fileId');
    fireEvent.change(fileIdField, { target: { value: dWFileMock.fileId } });
    const fileNameField = await findByLabelText('entities.dWFile.fileName');
    fireEvent.change(fileNameField, { target: { value: dWFileMock.fileName } });
    if (dWFileMock.folderFlag) {
      const folderFlagField = await findByTestId('dWFile-folderFlag-checkbox');
      fireEvent.click(folderFlagField);
    }
    const sizeField = await findByLabelText('entities.dWFile.size');
    fireEvent.change(sizeField, { target: { value: dWFileMock.size } });
    const downloadUrlField = await findByLabelText('entities.dWFile.downloadUrl');
    fireEvent.change(downloadUrlField, { target: { value: dWFileMock.downloadUrl } });
    const typeField = await findByLabelText('entities.dWFile.type');
    fireEvent.change(typeField, { target: { value: dWFileMock.type } });
    const parentIdField = await findByLabelText('entities.dWFile.parentId');
    fireEvent.change(parentIdField, { target: { value: dWFileMock.parentId } });
    if (dWFileMock.inTrash) {
      const inTrashField = await findByTestId('dWFile-inTrash-checkbox');
      fireEvent.click(inTrashField);
    }
    const previewLinkField = await findByLabelText('entities.dWFile.previewLink');
    fireEvent.change(previewLinkField, { target: { value: dWFileMock.previewLink } });
    const createdDateField = await findByLabelText('entities.dWFile.createdDate');
    fireEvent.change(createdDateField, { target: { value: dWFileMock.createdDate } });
    const modifiedDateField = await findByLabelText('entities.dWFile.modifiedDate');
    fireEvent.change(modifiedDateField, { target: { value: dWFileMock.modifiedDate } });
    rerender(<DWFileAddFormContainer onError={onErrorMock} onUpdate={onCreateMock} />);

    const saveButton = await findByTestId('submit-btn');

    fireEvent.click(saveButton);

    await wait(() => {
      expect(apiDWFilePost).toHaveBeenCalledTimes(1);
      expect(apiDWFilePost).toHaveBeenCalledWith('', dWFileMock);

      expect(queryByText(successMessageKey)).toBeInTheDocument();

      expect(onErrorMock).toHaveBeenCalledTimes(0);
      expect(queryByText(errorMessageKey)).not.toBeInTheDocument();
    });
  }, 7000);

  it('shows an error if data is not successfully saved', async () => {
    apiDWFilePost.mockImplementation(() => Promise.reject());

    const { findByTestId, findByLabelText, queryByText, rerender } = render(
      <DWFileAddFormContainer onError={onErrorMock} onUpdate={onCreateMock} />
    );

    const fileIdField = await findByLabelText('entities.dWFile.fileId');
    fireEvent.change(fileIdField, { target: { value: dWFileMock.fileId } });
    const fileNameField = await findByLabelText('entities.dWFile.fileName');
    fireEvent.change(fileNameField, { target: { value: dWFileMock.fileName } });
    if (dWFileMock.folderFlag) {
      const folderFlagField = await findByTestId('dWFile-folderFlag-checkbox');
      fireEvent.click(folderFlagField);
    }
    const sizeField = await findByLabelText('entities.dWFile.size');
    fireEvent.change(sizeField, { target: { value: dWFileMock.size } });
    const downloadUrlField = await findByLabelText('entities.dWFile.downloadUrl');
    fireEvent.change(downloadUrlField, { target: { value: dWFileMock.downloadUrl } });
    const typeField = await findByLabelText('entities.dWFile.type');
    fireEvent.change(typeField, { target: { value: dWFileMock.type } });
    const parentIdField = await findByLabelText('entities.dWFile.parentId');
    fireEvent.change(parentIdField, { target: { value: dWFileMock.parentId } });
    if (dWFileMock.inTrash) {
      const inTrashField = await findByTestId('dWFile-inTrash-checkbox');
      fireEvent.click(inTrashField);
    }
    const previewLinkField = await findByLabelText('entities.dWFile.previewLink');
    fireEvent.change(previewLinkField, { target: { value: dWFileMock.previewLink } });
    const createdDateField = await findByLabelText('entities.dWFile.createdDate');
    fireEvent.change(createdDateField, { target: { value: dWFileMock.createdDate } });
    const modifiedDateField = await findByLabelText('entities.dWFile.modifiedDate');
    fireEvent.change(modifiedDateField, { target: { value: dWFileMock.modifiedDate } });
    rerender(<DWFileAddFormContainer onError={onErrorMock} onUpdate={onCreateMock} />);

    const saveButton = await findByTestId('submit-btn');

    fireEvent.click(saveButton);

    await wait(() => {
      expect(apiDWFilePost).toHaveBeenCalledTimes(1);
      expect(apiDWFilePost).toHaveBeenCalledWith('', dWFileMock);

      expect(queryByText(successMessageKey)).not.toBeInTheDocument();

      expect(onErrorMock).toHaveBeenCalledTimes(1);
      expect(queryByText(errorMessageKey)).toBeInTheDocument();
    });
  }, 7000);
});
