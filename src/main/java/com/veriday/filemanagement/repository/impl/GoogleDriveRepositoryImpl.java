package com.veriday.filemanagement.repository.impl;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.FileList;
import com.veriday.filemanagement.domain.DWFile;
import com.veriday.filemanagement.google.GoogleAuthHelper;
import com.veriday.filemanagement.google.GoogleFileTypes;
import com.veriday.filemanagement.repository. DWFileRepository;

import java.io.File;
import java.time.ZoneId;
import java.util.*;

import com.veriday.filemanagement.service.UnauthorizedException;
import org.springframework.stereotype.Repository;

@Repository
public class GoogleDriveRepositoryImpl implements DWFileRepository {

    private static final String APPLICATION_NAME = "Digital Workspace File Management";
    private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();

    private Drive getGoogleDrive(String userId) throws Exception {
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

        GoogleAuthHelper googleAuthHelper = new GoogleAuthHelper(userId);

        HttpRequestInitializer credential = googleAuthHelper.getGoogleCredential();

        if (credential == null) {
            throw new UnauthorizedException("please authorize first");
        }

        return new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
            .setApplicationName(APPLICATION_NAME)
            .build();
    }

    @Override
    public List<DWFile> listFilesInFolder(String userId, String folderId) throws Exception {
        Drive drive = getGoogleDrive(userId);

        String pageToken = null;

        String query = "'" + folderId + "' in parents and trashed = false";

        String fields = "nextPageToken, files(id, name, mimeType, size, parents, webContentLink, thumbnailLink, createdTime, modifiedTime)";

        List<DWFile> results = new ArrayList<>();

        do {
            FileList result = drive.files().list()
                .setQ(query)
                .setSpaces("drive")
                .setFields(fields)
                .setPageToken(pageToken)
                .execute();

            for (com.google.api.services.drive.model.File file : result.getFiles()) {
                DWFile dwFile =  new DWFile();

                dwFile.setFileId(file.getId());
                dwFile.setFileName(file.getName());
                dwFile.setFolderFlag(GoogleFileTypes.GOOGLE_APPLICATION_FOLDER.equals(file.getMimeType()));

                Long size = file.getSize();

                if (size == null) {
                    size = -1L;
                }

                dwFile.setSize(size);

                dwFile.setDownloadUrl(file.getWebContentLink());
                dwFile.setType(file.getMimeType());

                List<String> parents = file.getParents();

                String parent = "-1";

                if (parents != null) {
                    parent = parents.get(0);
                }

                dwFile.setParentId(parent);
                dwFile.setInTrash(false);

                dwFile.setPreviewLink(file.getThumbnailLink());

                DateTime createDateTime = file.getCreatedTime();

                Date createdDate = new Date(createDateTime.getValue());

                dwFile.setCreatedDate(createdDate.toInstant().atZone(ZoneId.systemDefault()));

                DateTime modifiedDateTime = file.getModifiedTime();

                Date modifiedDate = new Date(modifiedDateTime.getValue());

                dwFile.setModifiedDate(modifiedDate.toInstant().atZone(ZoneId.systemDefault()));

                results.add(dwFile);
            }
        } while (pageToken != null);

        return results;
    }

    @Override
    public Optional<DWFile> findById(String userId, String fileId) throws Exception {
        Drive drive = getGoogleDrive(userId);

        String fields = "id, name, mimeType, size, parents, webContentLink, thumbnailLink, createdTime, modifiedTime";

        com.google.api.services.drive.model.File file = drive.files().get(fileId).setFields(fields).execute();

        DWFile dwFile =  new DWFile();

        dwFile.setFileId(file.getId());
        dwFile.setFileName(file.getName());
        dwFile.setFolderFlag(GoogleFileTypes.GOOGLE_APPLICATION_FOLDER.equals(file.getMimeType()));

        Long size = file.getSize();

        if (size == null) {
            size = -1L;
        }

        dwFile.setSize(size);

        dwFile.setDownloadUrl(file.getWebContentLink());
        dwFile.setType(file.getMimeType());

        List<String> parents = file.getParents();

        String parent = "-1";

        if (parents != null) {
            parent = parents.get(0);
        }

        dwFile.setParentId(parent);
        dwFile.setInTrash(false);

        dwFile.setPreviewLink(file.getThumbnailLink());

        DateTime createDateTime = file.getCreatedTime();

        Date createdDate = new Date(createDateTime.getValue());

        dwFile.setCreatedDate(createdDate.toInstant().atZone(ZoneId.systemDefault()));

        DateTime modifiedDateTime = file.getModifiedTime();

        Date modifiedDate = new Date(modifiedDateTime.getValue());

        dwFile.setModifiedDate(modifiedDate.toInstant().atZone(ZoneId.systemDefault()));

        return Optional.of(dwFile);
    }

    @Override
    public Optional<DWFile> saveFolderInFolder(
        String userId, String parentFolderId, String subFolderName)
        throws Exception {

        Drive drive = getGoogleDrive(userId);

        com.google.api.services.drive.model.File subFolder = new com.google.api.services.drive.model.File();

        subFolder.setName(subFolderName);
        subFolder.setMimeType("application/vnd.google-apps.folder");
        subFolder.setParents(Collections.singletonList(parentFolderId));

        com.google.api.services.drive.model.File subFolderResult = drive.files().create(subFolder).execute();

        String folderId = subFolderResult.getId();

        return findById(userId, folderId);
    }

    @Override
    public Optional<DWFile> uploadFile(
        String userId, File uploadFile, String fileName, String fileType, String folderId)
        throws Exception {

        Drive drive = getGoogleDrive(userId);

        FileContent fileContent = new FileContent(fileType, uploadFile);

        List<String> parentIds = new ArrayList<>();

        parentIds.add(folderId);

        com.google.api.services.drive.model.File metadata = new com.google.api.services.drive.model.File()
            .setParents(parentIds)
            .setMimeType(fileType)
            .setName(fileName);

        Drive.Files.Create uploadedFile = drive.files().create(metadata, fileContent);

        MediaHttpUploader mediaHttpUploader = uploadedFile.getMediaHttpUploader();

        mediaHttpUploader.setChunkSize(MediaHttpUploader.MINIMUM_CHUNK_SIZE);

        mediaHttpUploader.setDirectUploadEnabled(false);

        mediaHttpUploader.setProgressListener(uploader -> System.out.println("progress: " + uploader.getProgress()));

        uploadedFile.execute();

        return Optional.of(new DWFile());
    }

    @Override
    public Optional<DWFile> deleteFile(String userId, String fileId) throws Exception {
        Drive drive = getGoogleDrive(userId);

        Optional<DWFile> dwFile = findById(userId, fileId);

        drive.files().delete(fileId).execute();

        return dwFile;
    }

}
