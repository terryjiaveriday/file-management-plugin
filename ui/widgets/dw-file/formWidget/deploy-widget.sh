INLINE_RUNTIME_CHUNK=false npm run build

pushd build/static/js

mv -f 2*.js vendor.dWFile-form.js
mv -f main*.js main.dWFile-form.js
mv -f runtime~main*.js runtime.dWFile-form.js

popd

serve -l 5001 build
