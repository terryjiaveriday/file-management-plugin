package com.veriday.filemanagement.service.impl;

import com.veriday.filemanagement.domain.DWFile;

import com.veriday.filemanagement.service.DWFileService;

import com.veriday.filemanagement.repository.DWFileRepository;

import java.io.File;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class GoogleDriveServiceImpl implements DWFileService {

    private DWFileRepository repository;

    @Autowired
    public GoogleDriveServiceImpl(DWFileRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<DWFile> findOne(String userId, String fileId) throws Exception {
        return this.repository.findById(userId, fileId);
    }

    @Override
    public List<DWFile> listFilesInFolder(String userId, String folderId) throws Exception {
        return this.repository.listFilesInFolder(userId, folderId);
    }

    @Override
    public Optional<DWFile> saveFolderInFolder(
        String userId, String parentFolderId, String subFolderName) throws Exception {
        return this.repository.saveFolderInFolder(userId, parentFolderId, subFolderName);
    }

    @Override
    public Optional<DWFile> uploadFile(
        String userId, File uploadFile, String fileName, String fileType, String folderId) throws Exception {
        return this.repository.uploadFile(userId, uploadFile, fileName, fileType, folderId);
    }

    @Override
    public Optional<DWFile> deleteFile(String userId, String folderId) throws Exception {
        return this.repository.deleteFile(userId, folderId);
    }

}
