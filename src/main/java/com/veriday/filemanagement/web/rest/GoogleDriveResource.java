

package com.veriday.filemanagement.web.rest;

import com.veriday.filemanagement.domain.DWFile;
import com.veriday.filemanagement.service.DWFileService;

import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Optional;

/**
* REST controller for managing {@link com.veriday.filemanagement.domain.DWFile}.
*/
@RestController
@RequestMapping("/api/google-drive")
public class GoogleDriveResource {

    private final Logger log = LoggerFactory.getLogger(GoogleDriveResource.class);

    private final DWFileService dwFileService;

    public GoogleDriveResource(DWFileService dwFileService) {
        this.dwFileService = dwFileService;
    }

    @GetMapping("/files/{userId}/{folderId}")
    public ResponseEntity<List<DWFile>> getFilesInFolder(
            @PathVariable String userId, @PathVariable String folderId)
        throws Exception {

        List<DWFile> dwFiles = dwFileService.listFilesInFolder(userId, folderId);

        return ResponseEntity.ok().body(dwFiles);
    }

    @GetMapping("/file/{userId}/{fileId}")
    public ResponseEntity<DWFile> getFile(@PathVariable String userId, @PathVariable String fileId) throws Exception {
        log.debug("REST request to get DWFile : {}", fileId);

        Optional<DWFile> dwFile = dwFileService.findOne(userId, fileId);

        return ResponseUtil.wrapOrNotFound(dwFile);
    }

    @DeleteMapping("/file/{userId}/{fileId}")
    public ResponseEntity<DWFile> deleteFile(@PathVariable String userId, @PathVariable String fileId) throws Exception {
        log.debug("REST request to get DWFile : {}", fileId);

        Optional<DWFile> dwFile = dwFileService.deleteFile(userId, fileId);

        return ResponseUtil.wrapOrNotFound(dwFile);
    }

    @PostMapping("/file/{userId}/{folderId}")
    public void uploadFileInFolder(
        @PathVariable String userId, @PathVariable String folderId, @RequestParam("file") MultipartFile file)
        throws Exception {

        String fileName = file.getOriginalFilename();

        InputStream inputStream = file.getInputStream();

        Path tempFilePath = Files.createTempFile("dwfile", "");

        String fileType = file.getContentType();

        Files.copy(inputStream, tempFilePath, StandardCopyOption.REPLACE_EXISTING);

        File tempFile = tempFilePath.toFile();

        dwFileService.uploadFile(userId, tempFile, fileName, fileType, folderId);

        tempFile.deleteOnExit();
    }

    @PostMapping("/folder/{userId}/{parentFolderId}/{folderName}")
    public ResponseEntity<DWFile> createFolderInFolder(
        HttpServletResponse response, @PathVariable String userId, @PathVariable String parentFolderId,
        @PathVariable String folderName)
        throws Exception {

        response.setContentType(MediaType.APPLICATION_JSON_VALUE);

        Optional<DWFile> dwFile = dwFileService.saveFolderInFolder(userId, parentFolderId, folderName);

        return ResponseUtil.wrapOrNotFound(dwFile);
    }

}
