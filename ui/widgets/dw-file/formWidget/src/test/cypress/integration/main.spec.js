import { customElementName } from '../support';

describe('Main', () => {
  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then((oauth2Data) => {
      cy.keycloackLogin(oauth2Data, 'user');
    });
  });

  afterEach(() => {
    cy.get('@oauth2Data').then((oauth2Data) => {
      cy.keycloackLogout(oauth2Data);
    });
    cy.clearCache();
  });

  describe('Form widget', () => {
    it('should load the page', () => {
      cy.get(customElementName).should('exist');
    });

    it('should display all the entity fields in the component', () => {
      cy.contains('entities.dWFile.fileId').should('be.visible');
      cy.contains('entities.dWFile.fileName').should('be.visible');
      cy.contains('entities.dWFile.folderFlag').should('be.visible');
      cy.contains('entities.dWFile.size').should('be.visible');
      cy.contains('entities.dWFile.downloadUrl').should('be.visible');
      cy.contains('entities.dWFile.type').should('be.visible');
      cy.contains('entities.dWFile.parentId').should('be.visible');
      cy.contains('entities.dWFile.inTrash').should('be.visible');
      cy.contains('entities.dWFile.previewLink').should('be.visible');
      cy.contains('entities.dWFile.createdDate').should('be.visible');
      cy.contains('entities.dWFile.modifiedDate').should('be.visible');
    });
  });
});
