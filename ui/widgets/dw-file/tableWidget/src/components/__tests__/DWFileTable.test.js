import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render } from '@testing-library/react';
import i18n from 'components/__mocks__/i18n';
import dWFileMocks from 'components/__mocks__/dWFileMocks';
import DWFileTable from 'components/DWFileTable';

describe('DWFileTable', () => {
  it('shows dWFiles', () => {
    const { getByText } = render(<DWFileTable items={dWFileMocks} />);

    expect(getByText(dWFileMocks[0].fileId)).toBeInTheDocument();
    expect(getByText(dWFileMocks[1].fileId)).toBeInTheDocument();

    expect(getByText(dWFileMocks[0].fileName)).toBeInTheDocument();
    expect(getByText(dWFileMocks[1].fileName)).toBeInTheDocument();

    expect(getByText(dWFileMocks[0].size.toString())).toBeInTheDocument();
    expect(getByText(dWFileMocks[1].size.toString())).toBeInTheDocument();

    expect(getByText(dWFileMocks[0].downloadUrl)).toBeInTheDocument();
    expect(getByText(dWFileMocks[1].downloadUrl)).toBeInTheDocument();

    expect(getByText(dWFileMocks[0].type)).toBeInTheDocument();
    expect(getByText(dWFileMocks[1].type)).toBeInTheDocument();

    expect(getByText(dWFileMocks[0].parentId)).toBeInTheDocument();
    expect(getByText(dWFileMocks[1].parentId)).toBeInTheDocument();

    expect(getByText(dWFileMocks[0].previewLink)).toBeInTheDocument();
    expect(getByText(dWFileMocks[1].previewLink)).toBeInTheDocument();

    expect(
      getByText(new Date(dWFileMocks[0].createdDate).toLocaleString(i18n.language))
    ).toBeInTheDocument();
    expect(
      getByText(new Date(dWFileMocks[1].createdDate).toLocaleString(i18n.language))
    ).toBeInTheDocument();

    expect(
      getByText(new Date(dWFileMocks[0].modifiedDate).toLocaleString(i18n.language))
    ).toBeInTheDocument();
    expect(
      getByText(new Date(dWFileMocks[1].modifiedDate).toLocaleString(i18n.language))
    ).toBeInTheDocument();
  });

  it('shows no dWFiles message', () => {
    const { queryByText } = render(<DWFileTable items={[]} />);

    expect(queryByText(dWFileMocks[0].fileId)).not.toBeInTheDocument();
    expect(queryByText(dWFileMocks[1].fileId)).not.toBeInTheDocument();

    expect(queryByText(dWFileMocks[0].fileName)).not.toBeInTheDocument();
    expect(queryByText(dWFileMocks[1].fileName)).not.toBeInTheDocument();

    expect(queryByText(dWFileMocks[0].size.toString())).not.toBeInTheDocument();
    expect(queryByText(dWFileMocks[1].size.toString())).not.toBeInTheDocument();

    expect(queryByText(dWFileMocks[0].downloadUrl)).not.toBeInTheDocument();
    expect(queryByText(dWFileMocks[1].downloadUrl)).not.toBeInTheDocument();

    expect(queryByText(dWFileMocks[0].type)).not.toBeInTheDocument();
    expect(queryByText(dWFileMocks[1].type)).not.toBeInTheDocument();

    expect(queryByText(dWFileMocks[0].parentId)).not.toBeInTheDocument();
    expect(queryByText(dWFileMocks[1].parentId)).not.toBeInTheDocument();

    expect(queryByText(dWFileMocks[0].previewLink)).not.toBeInTheDocument();
    expect(queryByText(dWFileMocks[1].previewLink)).not.toBeInTheDocument();

    expect(
      queryByText(new Date(dWFileMocks[0].createdDate).toLocaleString(i18n.language))
    ).not.toBeInTheDocument();
    expect(
      queryByText(new Date(dWFileMocks[1].createdDate).toLocaleString(i18n.language))
    ).not.toBeInTheDocument();

    expect(
      queryByText(new Date(dWFileMocks[0].modifiedDate).toLocaleString(i18n.language))
    ).not.toBeInTheDocument();
    expect(
      queryByText(new Date(dWFileMocks[1].modifiedDate).toLocaleString(i18n.language))
    ).not.toBeInTheDocument();

    expect(queryByText('entities.dWFile.noItems')).toBeInTheDocument();
  });

  it('calls onSelect when the user clicks a table row', () => {
    const onSelectMock = jest.fn();
    const { getByText } = render(<DWFileTable items={dWFileMocks} onSelect={onSelectMock} />);

    fireEvent.click(getByText(dWFileMocks[0].fileId));
    expect(onSelectMock).toHaveBeenCalledTimes(1);
  });
});
