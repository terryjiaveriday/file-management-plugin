import React from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';

import dWFileType from 'components/__types__/dWFile';

const styles = {
  tableRoot: {
    marginTop: '10px',
  },
  rowRoot: {
    cursor: 'pointer',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
  noItems: {
    margin: '15px',
  },
};

const DWFileTable = ({ items, onSelect, classes, t, i18n, Actions }) => {
  const tableRows = items.map((item) => (
    <TableRow hover className={classes.rowRoot} key={item.id} onClick={() => onSelect(item)}>
      <TableCell>
        <span>{item.fileId}</span>
      </TableCell>
      <TableCell>
        <span>{item.fileName}</span>
      </TableCell>
      <TableCell align="center">
        <Checkbox disabled checked={item.folderFlag} />
      </TableCell>
      <TableCell>
        <span>{item.size}</span>
      </TableCell>
      <TableCell>
        <span>{item.downloadUrl}</span>
      </TableCell>
      <TableCell>
        <span>{item.type}</span>
      </TableCell>
      <TableCell>
        <span>{item.parentId}</span>
      </TableCell>
      <TableCell align="center">
        <Checkbox disabled checked={item.inTrash} />
      </TableCell>
      <TableCell>
        <span>{item.previewLink}</span>
      </TableCell>
      <TableCell>
        <span>{new Date(item.createdDate).toLocaleString(i18n.language)}</span>
      </TableCell>
      <TableCell>
        <span>{new Date(item.modifiedDate).toLocaleString(i18n.language)}</span>
      </TableCell>
      {Actions && (
        <TableCell>
          <Actions item={item} />
        </TableCell>
      )}
    </TableRow>
  ));

  return items.length ? (
    <Table className={classes.tableRoot} stickyHeader>
      <TableHead>
        <TableRow>
          <TableCell>
            <span>{t('entities.dWFile.fileId')}</span>
          </TableCell>
          <TableCell>
            <span>{t('entities.dWFile.fileName')}</span>
          </TableCell>
          <TableCell>
            <span>{t('entities.dWFile.folderFlag')}</span>
          </TableCell>
          <TableCell>
            <span>{t('entities.dWFile.size')}</span>
          </TableCell>
          <TableCell>
            <span>{t('entities.dWFile.downloadUrl')}</span>
          </TableCell>
          <TableCell>
            <span>{t('entities.dWFile.type')}</span>
          </TableCell>
          <TableCell>
            <span>{t('entities.dWFile.parentId')}</span>
          </TableCell>
          <TableCell>
            <span>{t('entities.dWFile.inTrash')}</span>
          </TableCell>
          <TableCell>
            <span>{t('entities.dWFile.previewLink')}</span>
          </TableCell>
          <TableCell>
            <span>{t('entities.dWFile.createdDate')}</span>
          </TableCell>
          <TableCell>
            <span>{t('entities.dWFile.modifiedDate')}</span>
          </TableCell>
          {Actions && <TableCell />}
        </TableRow>
      </TableHead>
      <TableBody>{tableRows}</TableBody>
    </Table>
  ) : (
    <div className={classes.noItems}>{t('entities.dWFile.noItems')}</div>
  );
};

DWFileTable.propTypes = {
  items: PropTypes.arrayOf(dWFileType).isRequired,
  onSelect: PropTypes.func,
  classes: PropTypes.shape({
    rowRoot: PropTypes.string,
    tableRoot: PropTypes.string,
    noItems: PropTypes.string,
  }).isRequired,
  t: PropTypes.func.isRequired,
  i18n: PropTypes.shape({ language: PropTypes.string }).isRequired,
  Actions: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
};

DWFileTable.defaultProps = {
  onSelect: () => {},
  Actions: null,
};

export default withStyles(styles)(withTranslation()(DWFileTable));
