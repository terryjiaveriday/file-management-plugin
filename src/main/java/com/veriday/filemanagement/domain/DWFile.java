package com.veriday.filemanagement.domain;


import java.io.Serializable;
import java.time.ZonedDateTime;

/**
* A DWFile.
*/
public class DWFile implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String fileId;

    private String fileName;

    private Boolean folderFlag;

    private Long size;

    private String downloadUrl;

    private String type;

    private String parentId;

    private Boolean inTrash;

    private String previewLink;

    private ZonedDateTime createdDate;

    private ZonedDateTime modifiedDate;


    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getFileId() {
        return fileId;
    }


    public DWFile fileId(String fileId) {
        this.fileId = fileId;
        return this;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFileName() {
        return fileName;
    }


    public DWFile fileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Boolean isFolderFlag() {
        return folderFlag;
    }


    public DWFile folderFlag(Boolean folderFlag) {
        this.folderFlag = folderFlag;
        return this;
    }

    public void setFolderFlag(Boolean folderFlag) {
        this.folderFlag = folderFlag;
    }

    public Long getSize() {
        return size;
    }


    public DWFile size(Long size) {
        this.size = size;
        return this;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }


    public DWFile downloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
        return this;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getType() {
        return type;
    }


    public DWFile type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getParentId() {
        return parentId;
    }


    public DWFile parentId(String parentId) {
        this.parentId = parentId;
        return this;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Boolean isInTrash() {
        return inTrash;
    }


    public DWFile inTrash(Boolean inTrash) {
        this.inTrash = inTrash;
        return this;
    }

    public void setInTrash(Boolean inTrash) {
        this.inTrash = inTrash;
    }

    public String getPreviewLink() {
        return previewLink;
    }


    public DWFile previewLink(String previewLink) {
        this.previewLink = previewLink;
        return this;
    }

    public void setPreviewLink(String previewLink) {
        this.previewLink = previewLink;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }


    public DWFile createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public ZonedDateTime getModifiedDate() {
        return modifiedDate;
    }


    public DWFile modifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
        return this;
    }

    public void setModifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DWFile)) {
            return false;
        }
        return id != null && id.equals(((DWFile) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DWFile{" +
        "id=" + getId() +
        ", fileId='" + getFileId() + "'" +
        ", fileName='" + getFileName() + "'" +
        ", folderFlag='" + isFolderFlag() + "'" +
        ", size=" + getSize() +
        ", downloadUrl='" + getDownloadUrl() + "'" +
        ", type='" + getType() + "'" +
        ", parentId='" + getParentId() + "'" +
        ", inTrash='" + isInTrash() + "'" +
        ", previewLink='" + getPreviewLink() + "'" +
        ", createdDate='" + getCreatedDate() + "'" +
        ", modifiedDate='" + getModifiedDate() + "'" +
        "}";
    }
}
