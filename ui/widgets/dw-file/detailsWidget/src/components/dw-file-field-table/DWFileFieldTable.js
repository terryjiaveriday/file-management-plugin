import React from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';

import dWFileType from 'components/__types__/dWFile';

const DWFileFieldTable = ({ t, i18n: { language }, dWFile }) => (
  <Table>
    <TableHead>
      <TableRow>
        <TableCell>{t('common.name')}</TableCell>
        <TableCell>{t('common.value')}</TableCell>
      </TableRow>
    </TableHead>
    <TableBody>
      <TableRow>
        <TableCell>
          <span>{t('entities.dWFile.id')}</span>
        </TableCell>
        <TableCell data-testid="dWFileIdValue">
          <span>{dWFile.id}</span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.dWFile.fileId')}</span>
        </TableCell>
        <TableCell>
          <span>{dWFile.fileId}</span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.dWFile.fileName')}</span>
        </TableCell>
        <TableCell>
          <span>{dWFile.fileName}</span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.dWFile.folderFlag')}</span>
        </TableCell>
        <TableCell>
          <span>
            <Checkbox disabled checked={dWFile.folderFlag} />
          </span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.dWFile.size')}</span>
        </TableCell>
        <TableCell>
          <span>{dWFile.size}</span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.dWFile.downloadUrl')}</span>
        </TableCell>
        <TableCell>
          <span>{dWFile.downloadUrl}</span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.dWFile.type')}</span>
        </TableCell>
        <TableCell>
          <span>{dWFile.type}</span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.dWFile.parentId')}</span>
        </TableCell>
        <TableCell>
          <span>{dWFile.parentId}</span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.dWFile.inTrash')}</span>
        </TableCell>
        <TableCell>
          <span>
            <Checkbox disabled checked={dWFile.inTrash} />
          </span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.dWFile.previewLink')}</span>
        </TableCell>
        <TableCell>
          <span>{dWFile.previewLink}</span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.dWFile.createdDate')}</span>
        </TableCell>
        <TableCell>
          <span>{dWFile.createdDate && new Date(dWFile.createdDate).toLocaleString(language)}</span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.dWFile.modifiedDate')}</span>
        </TableCell>
        <TableCell>
          <span>
            {dWFile.modifiedDate && new Date(dWFile.modifiedDate).toLocaleString(language)}
          </span>
        </TableCell>
      </TableRow>
    </TableBody>
  </Table>
);

DWFileFieldTable.propTypes = {
  dWFile: dWFileType,
  t: PropTypes.func.isRequired,
  i18n: PropTypes.shape({
    language: PropTypes.string,
  }).isRequired,
};

DWFileFieldTable.defaultProps = {
  dWFile: [],
};

export default withTranslation()(DWFileFieldTable);
