const dWFileMock = {
  fileId:
    'Vero minima atque dolor est expedita reiciendis ipsam doloribus. Ea facere tempore voluptatibus placeat quam sed voluptas. Iusto saepe expedita quia accusamus consequuntur magnam aut. Amet et vero consequatur quisquam blanditiis dolorem. Sint maxime doloribus ipsum quam culpa. Occaecati consequatur ut.',
  fileName:
    'Consequatur voluptas a quam dolorum. Animi minima voluptas numquam. Velit sunt unde qui facere voluptatem. Magni perspiciatis ea voluptas et nulla adipisci molestiae accusamus dolor. Sed laborum sunt voluptatum. Iusto quas ducimus nesciunt cum quibusdam earum quidem voluptatem rerum.',
  folderFlag: true,
  size: 897,
  downloadUrl:
    'Voluptatem ea exercitationem eaque quisquam ea. Doloribus a nostrum adipisci dolorem perspiciatis sit cum quia. In et at sed suscipit molestiae molestiae sunt. Nostrum excepturi odit numquam. Molestias quis totam delectus soluta voluptate natus.',
  type:
    'Sint suscipit reiciendis voluptatem voluptas. Qui perferendis ratione quae similique quae laboriosam velit. Perspiciatis deserunt reprehenderit fugiat vel culpa iusto. Corporis animi rerum ut eligendi fugiat accusantium illum quis.',
  parentId:
    'Ipsum est sed inventore perspiciatis eveniet possimus sapiente vel est. Enim magni in dolore est asperiores aut. Ipsa magnam eaque velit sit quos dolor.',
  inTrash: false,
  previewLink:
    'Ipsam est sed. Laudantium non quo ipsam ex commodi. Et ea aut sit est facilis recusandae perspiciatis molestias. Voluptatem praesentium sed maiores nihil eos consequuntur nemo in. Ut cum iure dolorem.',
  createdDate: '1976-12-11T00:49:00-05:00',
  modifiedDate: '2009-02-06T18:17:46-05:00',
};

export default dWFileMock;
