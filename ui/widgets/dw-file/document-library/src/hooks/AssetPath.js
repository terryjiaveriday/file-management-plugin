import { createContext, useContext } from "react";

export function getPath() {
    return process.env.NODE_ENV === 'development' ? process.env.REACT_APP_ENTANDO_SERVER_URL  : '';
}
const AssetPathContext = createContext('');

const useAssetPath = () => useContext(AssetPathContext);

function AssetPathProvider({ children }) {

    const path = getPath();

    return (
        <AssetPathContext.Provider value={path}>
            {children}
        </AssetPathContext.Provider>
    )
}
export { AssetPathProvider, useAssetPath };