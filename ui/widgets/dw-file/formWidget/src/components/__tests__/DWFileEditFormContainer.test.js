import React from 'react';
import { fireEvent, render, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { apiDWFileGet, apiDWFilePut } from 'api/dWFiles';
import DWFileEditFormContainer from 'components/DWFileEditFormContainer';
import 'i18n/__mocks__/i18nMock';
import dWFileMock from 'components/__mocks__/dWFileMocks';

jest.mock('api/dWFiles');

jest.mock('auth/withKeycloak', () => {
  const withKeycloak = (Component) => {
    return (props) => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        keycloak={{
          initialized: true,
          authenticated: true,
        }}
      />
    );
  };

  return withKeycloak;
});

describe('DWFileEditFormContainer', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  const errorMessageKey = 'error.dataLoading';
  const successMessageKey = 'common.dataSaved';

  const onErrorMock = jest.fn();
  const onUpdateMock = jest.fn();

  it('loads data', async () => {
    apiDWFileGet.mockImplementation(() => Promise.resolve(dWFileMock));
    const { queryByText } = render(
      <DWFileEditFormContainer id="1" onError={onErrorMock} onUpdate={onUpdateMock} />
    );

    await wait(() => {
      expect(apiDWFileGet).toHaveBeenCalledTimes(1);
      expect(apiDWFileGet).toHaveBeenCalledWith('', '1');
      expect(queryByText(errorMessageKey)).not.toBeInTheDocument();
      expect(onErrorMock).toHaveBeenCalledTimes(0);
    });
  }, 7000);

  it('saves data', async () => {
    apiDWFileGet.mockImplementation(() => Promise.resolve(dWFileMock));
    apiDWFilePut.mockImplementation(() => Promise.resolve(dWFileMock));

    const { findByTestId, queryByText } = render(
      <DWFileEditFormContainer id="1" onError={onErrorMock} onUpdate={onUpdateMock} />
    );

    const saveButton = await findByTestId('submit-btn');

    fireEvent.click(saveButton);

    await wait(() => {
      expect(apiDWFilePut).toHaveBeenCalledTimes(1);
      expect(apiDWFilePut).toHaveBeenCalledWith('', dWFileMock);
      expect(queryByText(successMessageKey)).toBeInTheDocument();
      expect(onErrorMock).toHaveBeenCalledTimes(0);
      expect(queryByText(errorMessageKey)).not.toBeInTheDocument();
    });
  }, 7000);

  it('shows an error if data is not successfully loaded', async () => {
    apiDWFileGet.mockImplementation(() => Promise.reject());
    const { queryByText } = render(
      <DWFileEditFormContainer id="1" onError={onErrorMock} onUpdate={onUpdateMock} />
    );

    await wait(() => {
      expect(apiDWFileGet).toHaveBeenCalledTimes(1);
      expect(apiDWFileGet).toHaveBeenCalledWith('', '1');
      expect(onErrorMock).toHaveBeenCalledTimes(1);
      expect(queryByText(errorMessageKey)).toBeInTheDocument();
      expect(queryByText(successMessageKey)).not.toBeInTheDocument();
    });
  }, 7000);

  it('shows an error if data is not successfully saved', async () => {
    apiDWFileGet.mockImplementation(() => Promise.resolve(dWFileMock));
    apiDWFilePut.mockImplementation(() => Promise.reject());
    const { findByTestId, getByText } = render(
      <DWFileEditFormContainer id="1" onError={onErrorMock} />
    );

    const saveButton = await findByTestId('submit-btn');

    fireEvent.click(saveButton);

    await wait(() => {
      expect(apiDWFileGet).toHaveBeenCalledTimes(1);
      expect(apiDWFileGet).toHaveBeenCalledWith('', '1');

      expect(apiDWFilePut).toHaveBeenCalledTimes(1);
      expect(apiDWFilePut).toHaveBeenCalledWith('', dWFileMock);

      expect(onErrorMock).toHaveBeenCalledTimes(1);
      expect(getByText(errorMessageKey)).toBeInTheDocument();
    });
  }, 7000);
});
