
import { useState, useEffect } from 'react';
import { filter } from 'lodash';
import { Icon } from '@iconify/react';

// material
import { Container, Grid, Box, Stack, IconButton, Drawer, Divider, useTheme,
    Card,
    Button,
    Checkbox,
    Table,
    TableRow,
    TableBody,
    TableCell,
    Typography,
    TableContainer,
    TablePagination  } from '@mui/material';
import { styled } from '@mui/material/styles';
import downloadFill from '@iconify/icons-eva/download-fill';
import InfoIcon from '@mui/icons-material/Info';
import CloseIcon from '@mui/icons-material/Close';

// routes

import { PATH_DASHBOARD } from '../routes/paths';

// components

// import Page from '../components/Page';
import NavSection from '../components/NavSection';
import Scrollbar from '../components/Scrollbar';
import SearchNotFound from '../components/SearchNotFound';

import { FileListHead, FileListToolbar } from '../components/_dashboard/e-commerce/product-list';

// hooks
import useCollapseDrawer from '../hooks/useCollapseDrawer';
import { useAuth } from '../hooks/KeycloakContext';

// utils

import { fDate } from '../utils/formatTime';

// redux

import { useDispatch, useSelector } from '../redux/store';
import { getFiles, getFolderFiles } from '../redux/slices/file';

// ----------------------------------------------------------------------


const librarySidebarConfig = [

    // MANAGEMENT
    // ----------------------------------------------------------------------
    {
      subheader: '',
      items: [
        // Library 1
        {
          title: 'Library 1',
          path: PATH_DASHBOARD.user.root,
          children: [
            { title: 'Folder1', path: PATH_DASHBOARD.user.profile },
            { title: 'Folder2', path: PATH_DASHBOARD.user.cards }
          ]
        },

        // Library 2
        {
          title: 'Library 2',
          path: PATH_DASHBOARD.eCommerce.root,
          children: [
            { title: 'Folder1', path: PATH_DASHBOARD.eCommerce.shop },
            { title: 'Folder2', path: PATH_DASHBOARD.eCommerce.productById },
            { title: 'Folder3', path: PATH_DASHBOARD.eCommerce.list },
            { title: 'Folder4', path: PATH_DASHBOARD.eCommerce.newProduct },
            { title: 'Folder5', path: PATH_DASHBOARD.eCommerce.editById },
            { title: 'Folder6', path: PATH_DASHBOARD.eCommerce.checkout },
            { title: 'Folder7', path: PATH_DASHBOARD.eCommerce.invoice }
          ]
        },

        // Library 3
        {
            title: 'Library 3',
            path: PATH_DASHBOARD.eCommerce.root,
            children: [
              { title: 'Folder1', path: PATH_DASHBOARD.eCommerce.shop },
              { title: 'Folder2', path: PATH_DASHBOARD.eCommerce.productById },
              { title: 'Folder3', path: PATH_DASHBOARD.eCommerce.list },
              { title: 'Folder4', path: PATH_DASHBOARD.eCommerce.newProduct },
              { title: 'Folder5', path: PATH_DASHBOARD.eCommerce.editById },
              { title: 'Folder6', path: PATH_DASHBOARD.eCommerce.checkout },
              { title: 'Folder7', path: PATH_DASHBOARD.eCommerce.invoice }
            ]
          },

          // Library 4
          {
            title: 'Library 4',
            path: PATH_DASHBOARD.eCommerce.root,
            children: [
              { title: 'Folder1', path: PATH_DASHBOARD.eCommerce.shop },
              { title: 'Folder2', path: PATH_DASHBOARD.eCommerce.productById },
              { title: 'Folder3', path: PATH_DASHBOARD.eCommerce.list },
              { title: 'Folder4', path: PATH_DASHBOARD.eCommerce.newProduct },
              { title: 'Folder5', path: PATH_DASHBOARD.eCommerce.editById },
              { title: 'Folder6', path: PATH_DASHBOARD.eCommerce.checkout },
              { title: 'Folder7', path: PATH_DASHBOARD.eCommerce.invoice }
            ]
          },

          // Library 5
          {
            title: 'Library 5',
            path: PATH_DASHBOARD.eCommerce.root,
            children: [
              { title: 'Folder1', path: PATH_DASHBOARD.eCommerce.shop },
              { title: 'Folder2', path: PATH_DASHBOARD.eCommerce.productById },
              { title: 'Folder3', path: PATH_DASHBOARD.eCommerce.list },
              { title: 'Folder4', path: PATH_DASHBOARD.eCommerce.newProduct },
              { title: 'Folder5', path: PATH_DASHBOARD.eCommerce.editById },
              { title: 'Folder6', path: PATH_DASHBOARD.eCommerce.checkout },
              { title: 'Folder7', path: PATH_DASHBOARD.eCommerce.invoice }
            ]
          }
      ]
    }
  ];


const TABLE_HEAD = [
    { id: 'FileTypeImage', align:'center' },
    { id: 'name', label: 'Name', alignRight: false },
    { id: 'createdAt', label: 'Created Date', alignRight: false },
    { id: '' }
  ];

const ThumbImgStyle = styled('img')(({ theme }) => ({
    width: 64,
    height: 64,
    objectFit: 'cover',
    margin: theme.spacing(0, 2),
    borderRadius: theme.shape.borderRadiusSm
  }));

function descendingComparator(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }

function getComparator(order, orderBy) {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }

function applySortFilter(array, comparator, query) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });

    if (query) {
      return filter(array, (_file) => _file.name.toLowerCase().indexOf(query.toLowerCase()) !== -1);
    }

    return stabilizedThis.map((el) => el[0]);
  }

  const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  }));

  const drawerWidth = 240;

export default function Library() {

  const keycloak = useAuth();

  const theme = useTheme();

  const dispatch = useDispatch();

  const { isCollapse } = useCollapseDrawer();

  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [selected, setSelected] = useState([]);
  const [filterName, setFilterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [orderBy, setOrderBy] = useState('createdAt');
  const { files } = useSelector((state) => state.product);
  const { folderFiles } = useSelector((state) => state.product);

  const [open, setOpen] = useState(false);

  // useEffect(() => {
  //   dispatch(getFiles());
  // }, [dispatch, keycloak.initialized, keycloak.authenticated]);

  useEffect(() => {
    dispatch(getFolderFiles());
  }, [dispatch,keycloak.initialized, keycloak.authenticated]);

  console.log('folderFiles', folderFiles)
  const handleChangeFolder = (event, fileId) => {

  }

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = files.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
    }
    setSelected(newSelected);
  };

  const handleDownload = (event, downloadUrl) => {
    window.open(downloadUrl);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - files.length) : 0;

  const filteredFiles = applySortFilter(files, getComparator(order, orderBy), filterName);

  const subFilteredFiles = filteredFiles;

  const isFileNotFound = files && files.length === 0;

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
      <Container maxWidth={false}
      sx={{  height: '100%', p: 5, m: 5 }}
      >

        <Grid
          container
          spacing={3}
          // justifyContent='right'
          >
            <Button
                type="button"
                variant="contained"
                size="large"
                sx={{ mr: 7 }}
                >
                Upload Documents
            </Button>

        </Grid>
        <Grid
          container
          spacing={3}
          alignItems="left"
          justifyContent={{ xs: 'left', md: 'space-between' }}
          sx={{ height: '100%' }}>
            <Grid item xs={2}>
                <Container  sx={{ height: '100%' }}>
                    <NavSection navConfig={librarySidebarConfig} isShow={!isCollapse} />
                </Container>
            </Grid>

            <Grid item xs={open ? 7 : 10}>
                <Container>
                    <FileListToolbar numSelected={selected.length} filterName={filterName} onFilterName={handleFilterByName} />
                    <IconButton
                      color="inherit"
                      aria-label="open drawer"
                      onClick={handleDrawerOpen}
                      edge="start"
                      sx={{ mr: 2, ...(open && { display: 'none' }) }}
                    >
                      <InfoIcon />
                    </IconButton>
                    <Scrollbar>
                        <TableContainer sx={{ minWidth: 800 }}>
                        <Table>
                            <FileListHead
                            order={order}
                            orderBy={orderBy}
                            headLabel={TABLE_HEAD}
                            rowCount={subFilteredFiles && subFilteredFiles.length}
                            numSelected={selected.length}
                            onRequestSort={handleRequestSort}
                            onSelectAllClick={handleSelectAllClick}
                            />
                            <TableBody>
                            {subFilteredFiles && subFilteredFiles.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
                                const { fileId, fileName, folderFlag, parent, previewLink, createdDate, type, downloadUrl } = row;

                                const isItemSelected = selected.indexOf(fileName) !== -1;

                                var previewLinkImage = '/static/mock-images/document-library/file_3.png';

                                if (folderFlag) {
                                  previewLinkImage = '/static/mock-images/document-library/folder_icon.png';
                                }

                                if (!type.startsWith("application/vnd.google-apps")) {
                                  previewLinkImage = previewLink;
                                }

                                return (
                                <TableRow
                                    hover
                                    key={fileId}
                                    tabIndex={-1}
                                    role="checkbox"
                                    selected={isItemSelected}
                                    aria-checked={isItemSelected}
                                >
                                    <TableCell padding="checkbox">
                                    <Checkbox checked={isItemSelected} onChange={(event) => handleClick(event, fileName)} />
                                    </TableCell>
                                    <TableCell style={{ minWidth: 100 }} onClick={(event => handleChangeFolder(event, fileId))}>
                                      <ThumbImgStyle alt={fileName} src={previewLinkImage} />
                                    </TableCell>
                                    <TableCell component="th" scope="row" padding="none">
                                    <Box
                                        sx={{
                                        py: 2,
                                        display: 'flex',
                                        alignItems: 'center'
                                        }}
                                    >
                                        <Typography variant="subtitle2" noWrap>
                                        {fileName}
                                        </Typography>
                                    </Box>
                                    </TableCell>
                                    <TableCell style={{ minWidth: 160 }}>{fDate(createdDate)}</TableCell>

                                    <TableCell align="center">
                                    {downloadUrl?
                                      <IconButton onClick={(event) => handleDownload(event, downloadUrl)}>
                                          <Icon icon={downloadFill} width={20} height={20}/>
                                      </IconButton>
                                    :""
                                    }

                                    </TableCell>
                                </TableRow>
                                );
                            })}
                            {emptyRows > 0 && (
                                <TableRow style={{ height: 53 * emptyRows }}>
                                <TableCell colSpan={6} />
                                </TableRow>
                            )}
                            </TableBody>
                            {isFileNotFound && (
                            <TableBody>
                                <TableRow>
                                <TableCell align="center" colSpan={6}>
                                    <Box sx={{ py: 3 }}>
                                    <SearchNotFound searchQuery={filterName} />
                                    </Box>
                                </TableCell>
                                </TableRow>
                            </TableBody>
                            )}
                        </Table>
                        </TableContainer>
                    </Scrollbar>

                    <TablePagination
                        rowsPerPageOptions={[5, 10, 25]}
                        component="div"
                        count={subFilteredFiles && subFilteredFiles.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onPageChange={handleChangePage}
                        onRowsPerPageChange={handleChangeRowsPerPage}
                    />
                </Container>
            </Grid>
            <Grid item xs={3}>


                <Drawer
                sx={{
                  width: drawerWidth,
                  flexShrink: 0,
                  '& .MuiDrawer-paper': {
                    width: drawerWidth,
                    boxSizing: 'border-box',
                  },
                }}
                variant="persistent"
                anchor="right"
                open={open}
                >
                  <DrawerHeader>
                    <IconButton onClick={handleDrawerClose}>
                      <CloseIcon />
                    </IconButton>
                  </DrawerHeader>
                  <Divider />
                  <Container>

                    <Stack spacing={3} sx={{ my: 3 }}>
                        <Stack item direction="row" sx={{ p: 3 }}  >
                            <ThumbImgStyle alt='docType' src='/static/mock-images/document-library/file_4.png' />
                            <Typography paragraph variant='h6' sx={{ my : 2}}>
                                Moon Fever
                            </Typography>
                        </Stack>

                    </Stack>
                    <Container style={{ backgroundColor: '#E5E8EB'}}>
                        <Box sx={{ position: 'relative', pt: 'calc(100% / 16 * 9)' }}>
                                <Box
                                    component="img"
                                    alt="media"
                                    src="/static/mock-images/document-library/sample-file.png"
                                    sx={{
                                    top: 1,
                                    width: 1,
                                    height: 1,
                                    borderRadius: 1,
                                    objectFit: 'contain',
                                    position: 'absolute',
                                    padding: 1
                                    }}
                                />
                        </Box>
                    </Container>
                    <Stack spacing={3} sx={{ my: 3 }}>
                        <Card>
                            <Stack direction="column" sx={{ p: 3 }} >
                                <Typography variant='h4'>
                                    Related Data
                                </Typography>
                            </Stack>
                            <Stack direction="column" alignItems="left" sx={{ p: 3 }}>
                                <Grid container>
                                    <Grid item xs={12} sm={6} >
                                        <Typography paragraph variant="subtitle1" sx={{ mt: 0.5 }}>
                                            Name
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12} sm={6} >
                                        <Typography paragraph variant="body2" sx={{ mt: 0.5 }}>
                                            Moon Fever
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12} sm={6} >
                                        <Typography paragraph variant="subtitle1" sx={{ mt: 0.5 }}>
                                            Uploaded By
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12} sm={6} >
                                    <Typography paragraph variant="body2" sx={{ mt: 0.5 }}>
                                        Travis Scott
                                    </Typography>
                                    </Grid>
                                    <Grid item xs={12} sm={6} >
                                        <Typography paragraph variant="subtitle1" sx={{ mt: 0.5 }}>
                                            Updloaded Date
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12} sm={6} >
                                        <Typography paragraph variant="body2" sx={{ mt: 0.5 }}>
                                            23/12/2021
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12} sm={6} >
                                        <Typography paragraph variant="subtitle1" sx={{ mt: 0.5 }}>
                                            Size
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12} sm={6} >
                                        <Typography paragraph variant="body2" sx={{ mt: 0.5 }}>
                                            323kb
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12} sm={6} >
                                        <Typography paragraph variant="subtitle1" sx={{ mt: 0.5 }}>
                                            Type
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12} sm={6} >
                                        <Typography paragraph variant="body2" sx={{ mt: 0.5 }}>
                                            PDF
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Stack>
                        </Card>
                    </Stack>

                    </Container>
                  <Divider />

                </Drawer>

            </Grid>
        </Grid>

      </Container>
  );
}
