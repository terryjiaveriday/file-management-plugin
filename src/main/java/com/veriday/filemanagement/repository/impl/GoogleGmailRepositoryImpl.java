package com.veriday.filemanagement.repository.impl;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;
import com.veriday.filemanagement.google.GoogleAuthHelper;
import com.veriday.filemanagement.repository.DWEmailRepository;
import com.veriday.filemanagement.service.UnauthorizedException;

import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Repository;

import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.util.Properties;

@Repository
public class GoogleGmailRepositoryImpl implements DWEmailRepository {

    private static final String APPLICATION_NAME = "Digital Workspace Email";
    private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();

    private Gmail getGoogleGmail(String userId) throws Exception {
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

        GoogleAuthHelper googleAuthHelper = new GoogleAuthHelper(userId);

        HttpRequestInitializer credential = googleAuthHelper.getGoogleCredential();

        if (credential == null) {
            throw new UnauthorizedException("please authorize first");
        }

        return new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
            .setApplicationName(APPLICATION_NAME)
            .build();
    }

    @Override
    public void sendMessageSimple(String userId, String to, String from, String subject, String bodyTest) throws Exception {
        Gmail gmail = getGoogleGmail(userId);

        Properties properties = new Properties();

        Session session = Session.getDefaultInstance(properties);

        MimeMessage mimeMessage = new MimeMessage(session);

        mimeMessage.setFrom(new InternetAddress(from));

        mimeMessage.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(to));

        mimeMessage.setSubject(subject);

        mimeMessage.setText(bodyTest);

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        mimeMessage.writeTo(buffer);

        byte[] bytes = buffer.toByteArray();

        String encodedEmail = Base64.encodeBase64URLSafeString(bytes);

        Message message = new Message();

        message.setRaw(encodedEmail);

        gmail.users().messages().send("me", message).execute();
    }

}
