import { getDefaultOptions, request } from 'api/helpers';

const resource = 'api/google-drive/file';

const userId = '0001';

/* eslint-disable-next-line import/prefer-default-export */
export const apiDWFileGet = async (serviceUrl, id) => {
  const url = `${serviceUrl}/${resource}/${userId}/${id}`;
  const options = {
    ...getDefaultOptions(),
    method: 'GET',
  };
  return request(url, options);
};
