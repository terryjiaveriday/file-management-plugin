package com.veriday.filemanagement.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.veriday.filemanagement.web.rest.TestUtil;

class DWFileTest {

@Test
void equalsVerifier() throws Exception {
TestUtil.equalsVerifier(DWFile.class);
    DWFile dWFile1 = new DWFile();
    dWFile1.setId(1L);
    DWFile dWFile2 = new DWFile();
    dWFile2.setId(dWFile1.getId());
    assertThat(dWFile1).isEqualTo(dWFile2);
    dWFile2.setId(2L);
    assertThat(dWFile1).isNotEqualTo(dWFile2);
    dWFile1.setId(null);
    assertThat(dWFile1).isNotEqualTo(dWFile2);
}
}
