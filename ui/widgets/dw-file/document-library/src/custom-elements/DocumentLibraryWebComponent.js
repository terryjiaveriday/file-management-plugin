import CustomElement from './CustomElement';
import Library from '../components/Library';
import { HelmetProvider } from 'react-helmet-async';
import { Provider as ReduxProvider } from 'react-redux';
import { store } from '../redux/store';
import { HashRouter } from 'react-router-dom';

const ATTRIBUTES = {
  serviceUrl: 'service-url',
  locale: 'locale'
};

class DocumentLibraryWebComponent extends CustomElement {
    static TAG = 'dw-document-library';
  
    constructor() {
      super(ATTRIBUTES, DocumentLibraryWebComponent);
    }
  
    get children() {
      const attrs = this.getAllAttributes();
      return <HelmetProvider>
                <ReduxProvider store={store}>
                <HashRouter>
                  <Library {...attrs} />
                 </HashRouter>
                </ReduxProvider>
              </HelmetProvider>
    }
  
    static get observedAttributes() {
      return Object.values(ATTRIBUTES);
    }
}

if (!customElements.get(DocumentLibraryWebComponent.TAG)) {
  customElements.define(DocumentLibraryWebComponent.TAG, DocumentLibraryWebComponent);
}
