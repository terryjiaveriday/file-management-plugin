import React from 'react';
import { render, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import 'components/__mocks__/i18n';
import { apiDWFileGet } from 'api/dWFile';
import dWFileApiGetResponseMock from 'components/__mocks__/dWFileMocks';
import DWFileDetailsContainer from 'components/DWFileDetailsContainer';

jest.mock('api/dWFile');

jest.mock('auth/withKeycloak', () => {
  const withKeycloak = Component => {
    return props => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        keycloak={{
          initialized: true,
          authenticated: true,
        }}
      />
    );
  };

  return withKeycloak;
});

beforeEach(() => {
  apiDWFileGet.mockClear();
});

describe('DWFileDetailsContainer component', () => {
  test('requests data when component is mounted', async () => {
    apiDWFileGet.mockImplementation(() => Promise.resolve(dWFileApiGetResponseMock));

    render(<DWFileDetailsContainer id="1" />);

    await wait(() => {
      expect(apiDWFileGet).toHaveBeenCalledTimes(1);
    });
  });

  test('data is shown after mount API call', async () => {
    apiDWFileGet.mockImplementation(() => Promise.resolve(dWFileApiGetResponseMock));

    const { getByText } = render(<DWFileDetailsContainer id="1" />);

    await wait(() => {
      expect(apiDWFileGet).toHaveBeenCalledTimes(1);
      expect(getByText('entities.dWFile.fileId')).toBeInTheDocument();
      expect(getByText('entities.dWFile.fileName')).toBeInTheDocument();
      expect(getByText('entities.dWFile.folderFlag')).toBeInTheDocument();
      expect(getByText('entities.dWFile.size')).toBeInTheDocument();
      expect(getByText('entities.dWFile.downloadUrl')).toBeInTheDocument();
      expect(getByText('entities.dWFile.type')).toBeInTheDocument();
      expect(getByText('entities.dWFile.parentId')).toBeInTheDocument();
      expect(getByText('entities.dWFile.inTrash')).toBeInTheDocument();
      expect(getByText('entities.dWFile.previewLink')).toBeInTheDocument();
      expect(getByText('entities.dWFile.createdDate')).toBeInTheDocument();
      expect(getByText('entities.dWFile.modifiedDate')).toBeInTheDocument();
    });
  });

  test('error is shown after failed API call', async () => {
    const onErrorMock = jest.fn();
    apiDWFileGet.mockImplementation(() => Promise.reject());

    const { getByText } = render(<DWFileDetailsContainer id="1" onError={onErrorMock} />);

    await wait(() => {
      expect(apiDWFileGet).toHaveBeenCalledTimes(1);
      expect(onErrorMock).toHaveBeenCalledTimes(1);
      expect(getByText('error.dataLoading')).toBeInTheDocument();
    });
  });
});
