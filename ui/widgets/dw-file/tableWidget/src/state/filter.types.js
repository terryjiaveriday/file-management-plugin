export const ADD_FILTER = 'dWFile-filter/addFilter';
export const UPDATE_FILTER = 'dWFile-filter/updateFilter';
export const DELETE_FILTER = 'dWFile-filter/deleteFilter';
export const CLEAR_FILTERS = 'dWFile-filter/clearFilters';
