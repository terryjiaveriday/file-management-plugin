package com.veriday.filemanagement.service;

import com.veriday.filemanagement.domain.DWFile;

import java.io.File;
import java.util.List;
import java.util.Optional;

public interface DWFileService {

    Optional<DWFile> findOne(String userId, String fileId) throws Exception;

    List<DWFile> listFilesInFolder(String userId, String folderId) throws Exception;

    Optional<DWFile> saveFolderInFolder(
        String userId, String parentFolderId, String subFolderName) throws Exception;

    Optional<DWFile> uploadFile(
        String userId, File uploadFile, String fileName, String fileType, String folderId) throws Exception;

    Optional<DWFile> deleteFile(String userId, String folderId) throws Exception;

}
