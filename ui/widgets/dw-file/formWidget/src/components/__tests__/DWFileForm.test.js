import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render, wait } from '@testing-library/react';
import i18n from 'i18n/__mocks__/i18nMock';
import dWFileMock from 'components/__mocks__/dWFileMocks';
import DWFileForm from 'components/DWFileForm';
import { createMuiTheme } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';

const theme = createMuiTheme();

describe('DWFile Form', () => {
  it('shows form', () => {
    const { getByLabelText } = render(
      <ThemeProvider theme={theme}>
        <DWFileForm dWFile={dWFileMock} />
      </ThemeProvider>
    );

    expect(getByLabelText('entities.dWFile.fileId').value).toBe(dWFileMock.fileId);
    expect(getByLabelText('entities.dWFile.fileName').value).toBe(dWFileMock.fileName);
    expect(getByLabelText('entities.dWFile.folderFlag').value).toBe('dWFile-active');
    expect(getByLabelText('entities.dWFile.size').value).toBe(dWFileMock.size.toString());
    expect(getByLabelText('entities.dWFile.downloadUrl').value).toBe(dWFileMock.downloadUrl);
    expect(getByLabelText('entities.dWFile.type').value).toBe(dWFileMock.type);
    expect(getByLabelText('entities.dWFile.parentId').value).toBe(dWFileMock.parentId);
    expect(getByLabelText('entities.dWFile.inTrash').value).toBe('dWFile-active');
    expect(getByLabelText('entities.dWFile.previewLink').value).toBe(dWFileMock.previewLink);
    expect(getByLabelText('entities.dWFile.createdDate').value).toBe(
      new Date(dWFileMock.createdDate).toLocaleString(i18n.language)
    );
    expect(getByLabelText('entities.dWFile.modifiedDate').value).toBe(
      new Date(dWFileMock.modifiedDate).toLocaleString(i18n.language)
    );
  });

  it('submits form', async () => {
    const handleSubmit = jest.fn();
    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <DWFileForm dWFile={dWFileMock} onSubmit={handleSubmit} />
      </ThemeProvider>
    );

    const form = getByTestId('dWFile-form');
    fireEvent.submit(form);

    await wait(() => {
      expect(handleSubmit).toHaveBeenCalledTimes(1);
    });
  });
});
