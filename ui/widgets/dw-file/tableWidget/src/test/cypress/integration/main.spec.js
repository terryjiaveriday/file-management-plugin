import { customElementName } from '../support';

describe('Main', () => {
  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then((oauth2Data) => {
      cy.keycloackLogin(oauth2Data, 'user');
    });
  });

  afterEach(() => {
    cy.get('@oauth2Data').then((oauth2Data) => {
      cy.keycloackLogout(oauth2Data);
    });
    cy.clearCache();
  });

  describe('Table widget', () => {
    it('should load the page', () => {
      cy.get(customElementName).should('exist');
    });

    it('should display all the entity fields in the component', () => {
      cy.contains('entities.dWFile.fileId').scrollIntoView().should('be.visible');
      cy.contains('entities.dWFile.fileName').scrollIntoView().should('be.visible');
      cy.contains('entities.dWFile.folderFlag').scrollIntoView().should('be.visible');
      cy.contains('entities.dWFile.size').scrollIntoView().should('be.visible');
      cy.contains('entities.dWFile.downloadUrl').scrollIntoView().should('be.visible');
      cy.contains('entities.dWFile.type').scrollIntoView().should('be.visible');
      cy.contains('entities.dWFile.parentId').scrollIntoView().should('be.visible');
      cy.contains('entities.dWFile.inTrash').scrollIntoView().should('be.visible');
      cy.contains('entities.dWFile.previewLink').scrollIntoView().should('be.visible');
      cy.contains('entities.dWFile.createdDate').scrollIntoView().should('be.visible');
      cy.contains('entities.dWFile.modifiedDate').scrollIntoView().should('be.visible');
    });
  });
});
