import React from 'react';
import ReactDOM from 'react-dom';
import { AuthProvider } from '../hooks/KeycloakContext'
import ThemeConfig from '../mui-theme/theme'
import ThemePrimaryColor from '../mui-theme/components/ThemePrimaryColor'
import { AssetPathProvider } from '../hooks/AssetPath';
import setLocale from '../i18n/setLocale'

const localeAttribute = 'locale';

export default class CustomElement extends HTMLElement {
  constructor(attributes, clazz) {
    super();
    this.attributez = attributes;
    this.clazz = clazz;
  }

  connectedCallback() {
    this.mountPoint = document.createElement('div');
    this.appendChild(this.mountPoint);
    this.render();
  }

  attributeChangedCallback(attribute, oldValue, newValue) {
    if (!this.clazz.observedAttributes.includes(attribute)) {
      throw new Error(`Untracked changed attribute: ${attribute}`);
    }
    if (this.mountPoint && newValue !== oldValue) {
      this.render();
    }
  }

  getAllAttributes() {
    const attrs = {};
    Object.entries(this.attributez).forEach(([key, value]) => {
      attrs[key] = this.getAttribute(value);
    })
    return attrs;
  }

  render() {
    const locale = this.getAttribute(localeAttribute);
    console.log(`In custom element: ${locale} for ${this.clazz.name}`)
    setLocale(locale);

    ReactDOM.render(
      <AuthProvider>
        <ThemeConfig>
          <AssetPathProvider>
            <ThemePrimaryColor>
              {this.children}
            </ThemePrimaryColor>
          </AssetPathProvider>
        </ThemeConfig>
      </AuthProvider >,
      this.mountPoint
    );
  }
}