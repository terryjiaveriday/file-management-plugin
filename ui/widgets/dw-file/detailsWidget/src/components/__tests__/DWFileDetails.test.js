import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';

import 'components/__mocks__/i18n';
import DWFileDetails from 'components/DWFileDetails';
import dWFileMock from 'components/__mocks__/dWFileMocks';

describe('DWFileDetails component', () => {
  test('renders data in details widget', () => {
    const { getByText } = render(<DWFileDetails dWFile={dWFileMock} />);

    expect(getByText('entities.dWFile.fileId')).toBeInTheDocument();
  });
});
