import PropTypes from 'prop-types';

export default PropTypes.shape({
  id: PropTypes.number,

  fileId: PropTypes.string,
  fileName: PropTypes.string,
  folderFlag: PropTypes.bool,
  size: PropTypes.number,
  downloadUrl: PropTypes.string,
  type: PropTypes.string,
  parentId: PropTypes.string,
  inTrash: PropTypes.bool,
  previewLink: PropTypes.string,
  createdDate: PropTypes.string,
  modifiedDate: PropTypes.string,
});

export const formValues = PropTypes.shape({
  fileId: PropTypes.string,
  fileName: PropTypes.string,
  folderFlag: PropTypes.bool,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  downloadUrl: PropTypes.string,
  type: PropTypes.string,
  parentId: PropTypes.string,
  inTrash: PropTypes.bool,
  previewLink: PropTypes.string,
  createdDate: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
  modifiedDate: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
});

export const formTouched = PropTypes.shape({
  fileId: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
  fileName: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
  folderFlag: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
  size: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
  downloadUrl: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
  type: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
  parentId: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
  inTrash: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
  previewLink: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
  createdDate: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
  modifiedDate: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
});

export const formErrors = PropTypes.shape({
  fileId: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
  fileName: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
  folderFlag: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
  downloadUrl: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
  type: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
  parentId: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
  inTrash: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
  previewLink: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
  createdDate: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
  modifiedDate: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
});
