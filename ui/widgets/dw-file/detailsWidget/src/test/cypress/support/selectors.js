const customElementName = 'dw-file-details';
const detailsTitle = '[data-testid=details_title]';
const entityIdCell = '[data-testid=dWFileIdValue]';

export { customElementName, detailsTitle, entityIdCell };
