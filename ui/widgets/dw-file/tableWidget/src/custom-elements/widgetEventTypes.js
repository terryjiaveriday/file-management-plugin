export const INPUT_EVENT_TYPES = {
  formUpdate: 'dWFile.form.update',
  formCreate: 'dWFile.form.create',
  formDelete: 'dWFile.form.delete',
};

export const OUTPUT_EVENT_TYPES = {
  select: 'dWFile.table.select',
  add: 'dWFile.table.add',
  error: 'dWFile.table.error',
  delete: 'dWFile.table.delete',
};

export const KEYCLOAK_EVENT_TYPE = 'keycloak';
