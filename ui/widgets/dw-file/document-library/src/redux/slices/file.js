import { sum, map, filter, uniqBy, reject } from 'lodash';
import { createSlice } from '@reduxjs/toolkit';
// utils
import axios from '../../utils/axios';
import { getDefaultOptions } from '../../_apis_/helpers';

import { request } from '../../_apis_/helpers'
// ----------------------------------------------------------------------

const initialState = {
  isLoading: false,
  error: false,
  files: [],
  file: null,
  sortBy: null,

};

const slice = createSlice({
  name: 'file',
  initialState,
  reducers: {
    // START LOADING
    startLoading(state) {
      state.isLoading = true;
    },

    // HAS ERROR
    hasError(state, action) {
      state.isLoading = false;
      state.error = action.payload;
    },

    // GET FILES
    getFilesSuccess(state, action) {
      state.isLoading = false;
      state.files = action.payload;
    },

    // GET FILE
    getFileSuccess(state, action) {
      state.isLoading = false;
      state.file = action.payload;
    },

    // DELETE FILE
    deleteFile(state, action) {
      state.filess = reject(state.files, { id: action.payload });
    },

    //  SORT & FILTER FILES
    sortByFiles(state, action) {
      state.sortBy = action.payload;
    },


  }
});

// Reducer
export default slice.reducer;

// Actions
export const {
  deleteFile,
  sortByFiles,
} = slice.actions;

// ----------------------------------------------------------------------

export function getFolderFiles(folderId) {
  return async (dispatch) => {
    dispatch(slice.actions.startLoading());
    try {

      const options = {
        ...getDefaultOptions(),
        method: 'GET',
      };
      console.log('running getfolderFiles ...')


      const response = await request('http://localhost:8081/services/fileManagement/api/google-drive/files/0001/root', options);
      console.log('response in fileFolder call => ', response)
      dispatch(slice.actions.getFilesSuccess(response));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}

export function getFiles() {
  return async (dispatch) => {
    dispatch(slice.actions.startLoading());
    try {

      const options = {
        ...getDefaultOptions(),
        method: 'GET',
      };
      // fileId: "1QYk6lrqXg2G7HaZiWLMggtsKmyY7OvPrU1-6RFsBeak"

      const response = await request('http://localhost:8081/services/fileManagement/api/google-drive/files/0001/QYk6lrqXg2G7HaZiWLMggtsKmyY7OvPrU1-6RFsBeak', options);
      dispatch(slice.actions.getFilesSuccess(response));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}

// ----------------------------------------------------------------------

export function getFile(name) {
  return async (dispatch) => {
    dispatch(slice.actions.startLoading());
    try {
      const response = await axios.get('/api/files/file', {
        params: { name }
      });
      dispatch(slice.actions.getFileSuccess(response));
    } catch (error) {
      console.error(error);
      dispatch(slice.actions.hasError(error));
    }
  };
}
