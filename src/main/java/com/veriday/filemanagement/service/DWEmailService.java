package com.veriday.filemanagement.service;

public interface DWEmailService {

    void sendMessageSimple(String userId, String to, String from, String subject, String bodyTest) throws Exception;

}
