import PropTypes from 'prop-types';

const dWFileType = PropTypes.shape({
  id: PropTypes.number,

  fileId: PropTypes.string,
  fileName: PropTypes.string,
  folderFlag: PropTypes.bool,
  size: PropTypes.number,
  downloadUrl: PropTypes.string,
  type: PropTypes.string,
  parentId: PropTypes.string,
  inTrash: PropTypes.bool,
  previewLink: PropTypes.string,
  createdDate: PropTypes.string,
  modifiedDate: PropTypes.string,
});

export default dWFileType;
