package com.veriday.filemanagement.service.impl;

import com.veriday.filemanagement.repository.DWEmailRepository;
import com.veriday.filemanagement.service.DWEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoogleGmailServiceImpl implements DWEmailService {

    private DWEmailRepository repository;

    @Autowired
    public GoogleGmailServiceImpl(DWEmailRepository repository) {
        this.repository = repository;
    }

    @Override
    public void sendMessageSimple(String userId, String to, String from, String subject, String bodyTest) throws Exception {
        this.repository.sendMessageSimple(userId, to, from, subject, bodyTest);
    }

}
