package com.veriday.filemanagement.web.rest;

import com.veriday.filemanagement.FileManagementApp;
import com.veriday.filemanagement.config.TestSecurityConfiguration;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
/**
* Integration tests for the {@link GoogleDriveResource} REST controller.
*/
@SpringBootTest(classes = { FileManagementApp.class, TestSecurityConfiguration.class })
@AutoConfigureMockMvc
@WithMockUser
public class DWFileResourceIT {

    @Autowired
    private MockMvc restDWFileMockMvc;

    @Test
    public void getAllDWFilesReturnsEmptyList() throws Exception {
        // Get all the dWFileList
        restDWFileMockMvc.perform(get("/api/dw-files"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(content().json("[]"));
    }

    @Test
    public void getNonExistingDWFile() throws Exception {
        // Get the dWFile
        restDWFileMockMvc.perform(get("/api/dw-files/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
}
}
