package com.veriday.filemanagement.service;

public class UnauthorizedException extends Exception {

    public UnauthorizedException(String msg) {
        super(msg);
    }

}
