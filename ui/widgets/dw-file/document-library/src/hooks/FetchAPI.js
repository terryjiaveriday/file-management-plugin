import { useCallback, useReducer, useState } from 'react';
import { useAuth } from './KeycloakContext';

export default function useFetch(keycloak, initialData) {
  const authHook = useAuth();
  const auth = keycloak || authHook;

  const [state, dispatch] = useReducer(fetchReducer, {
    isLoading: false,
    error: null,
    data: initialData
  });

  const doFetch = useCallback(async (request) => {
    if (!auth.initialized || !auth.authenticated) {
      console.log("Keycloak not initialized yet");
      return;
    }
    dispatch({ type: 'FETCH_INIT' });
    try {
      const jsonResponse = await request();
      dispatch({ type: 'FETCH_SUCCESS', payload: jsonResponse });
    }
    catch (error) {
      dispatch({ type: 'FETCH_ERROR', payload: error.message });
    }

  }, [auth && auth.authenticated]);

  return [{ ...state }, doFetch];
}

function fetchReducer(state, action) {
  switch (action.type) {
    case 'FETCH_INIT':
      return {
        ...state,
        isLoading: true,
        error: null,
      };
    case 'FETCH_SUCCESS':
      return {
        ...state,
        isLoading: false,
        error: null,
        data: action.payload
      };
    case 'FETCH_ERROR':
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    default:
      console.log("Wrong action type for FETCH API hook");
  }
}