package com.veriday.filemanagement.web.rest;

import com.veriday.filemanagement.service.DWEmailService;
import com.veriday.filemanagement.service.DWFileService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

@RestController
@RequestMapping("/api/google-gmail")
public class GoogleGmailResource {

    private final DWEmailService dwEmailService;

    public GoogleGmailResource(DWEmailService dwEmailService) {
        this.dwEmailService = dwEmailService;
    }

    @PostMapping("/message/{userId}")
    public void sendMessageSimple(
            @PathVariable String userId, @RequestParam("to") String to, @RequestParam("from") String from,
            @RequestParam("subject") String subject, @RequestParam("bodyTest") String bodyTest )
        throws Exception {

        dwEmailService.sendMessageSimple(userId, to, from, subject, bodyTest);
    }

}
