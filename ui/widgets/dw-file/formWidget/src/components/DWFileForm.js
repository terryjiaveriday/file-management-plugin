import 'date-fns';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { formValues, formTouched, formErrors } from 'components/__types__/dWFile';
import { withFormik } from 'formik';
import { withTranslation } from 'react-i18next';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';
import * as Yup from 'yup';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { DateTimePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import dateFnsLocales from 'i18n/dateFnsLocales';
import ConfirmationDialogTrigger from 'components/common/ConfirmationDialogTrigger';

const styles = (theme) => ({
  root: {
    margin: theme.spacing(3),
  },
  textField: {
    width: '100%',
  },
});
class DWFileForm extends PureComponent {
  constructor(props) {
    super(props);
    this.handleConfirmationDialogAction = this.handleConfirmationDialogAction.bind(this);
  }

  handleConfirmationDialogAction(action) {
    const { onDelete, values } = this.props;
    switch (action) {
      case ConfirmationDialogTrigger.CONFIRM: {
        onDelete(values);
        break;
      }
      default:
        break;
    }
  }

  render() {
    const {
      classes,
      values,
      touched,
      errors,
      handleChange,
      handleBlur,
      handleSubmit: formikHandleSubmit,
      onDelete,
      onCancelEditing,
      isSubmitting,
      setFieldValue,
      t,
      i18n,
    } = this.props;

    const handleDateChange = (field) => (value) => {
      setFieldValue(field, value);
    };

    const dateTimeLabelFn = (date) => (date ? new Date(date).toLocaleString(i18n.language) : '');
    const getHelperText = (field) => (errors[field] && touched[field] ? errors[field] : '');

    const handleSubmit = (e) => {
      e.stopPropagation(); // avoids double submission caused by react-shadow-dom-retarget-events
      formikHandleSubmit(e);
    };

    return (
      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={dateFnsLocales[i18n.language]}>
        <form onSubmit={handleSubmit} className={classes.root} data-testid="dWFile-form">
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                id="dWFile-fileId"
                error={errors.fileId && touched.fileId}
                helperText={getHelperText('fileId')}
                className={classes.textField}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.fileId}
                name="fileId"
                label={t('entities.dWFile.fileId')}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                id="dWFile-fileName"
                error={errors.fileName && touched.fileName}
                helperText={getHelperText('fileName')}
                className={classes.textField}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.fileName}
                name="fileName"
                label={t('entities.dWFile.fileName')}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControlLabel
                control={
                  // eslint-disable-next-line react/jsx-wrap-multilines
                  <Checkbox
                    id="dWFile-folderFlag"
                    name="folderFlag"
                    onChange={handleChange}
                    inputProps={{ 'data-testid': 'dWFile-folderFlag-checkbox' }}
                    checked={values.folderFlag}
                    value="dWFile-folderFlag"
                    color="primary"
                  />
                }
                label={t('entities.dWFile.folderFlag')}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                id="dWFile-size"
                error={errors.size && touched.size}
                helperText={getHelperText('size')}
                className={classes.textField}
                type="number"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.size}
                name="size"
                label={t('entities.dWFile.size')}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                id="dWFile-downloadUrl"
                error={errors.downloadUrl && touched.downloadUrl}
                helperText={getHelperText('downloadUrl')}
                className={classes.textField}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.downloadUrl}
                name="downloadUrl"
                label={t('entities.dWFile.downloadUrl')}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                id="dWFile-type"
                error={errors.type && touched.type}
                helperText={getHelperText('type')}
                className={classes.textField}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.type}
                name="type"
                label={t('entities.dWFile.type')}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                id="dWFile-parentId"
                error={errors.parentId && touched.parentId}
                helperText={getHelperText('parentId')}
                className={classes.textField}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.parentId}
                name="parentId"
                label={t('entities.dWFile.parentId')}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControlLabel
                control={
                  // eslint-disable-next-line react/jsx-wrap-multilines
                  <Checkbox
                    id="dWFile-inTrash"
                    name="inTrash"
                    onChange={handleChange}
                    inputProps={{ 'data-testid': 'dWFile-inTrash-checkbox' }}
                    checked={values.inTrash}
                    value="dWFile-inTrash"
                    color="primary"
                  />
                }
                label={t('entities.dWFile.inTrash')}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                id="dWFile-previewLink"
                error={errors.previewLink && touched.previewLink}
                helperText={getHelperText('previewLink')}
                className={classes.textField}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.previewLink}
                name="previewLink"
                label={t('entities.dWFile.previewLink')}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <DateTimePicker
                id="dWFile-createdDate"
                error={errors.createdDate && touched.createdDate}
                helperText={getHelperText('createdDate')}
                className={classes.textField}
                onChange={handleDateChange('createdDate')}
                value={values.createdDate}
                labelFunc={dateTimeLabelFn}
                name="createdDate"
                label={t('entities.dWFile.createdDate')}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <DateTimePicker
                id="dWFile-modifiedDate"
                error={errors.modifiedDate && touched.modifiedDate}
                helperText={getHelperText('modifiedDate')}
                className={classes.textField}
                onChange={handleDateChange('modifiedDate')}
                value={values.modifiedDate}
                labelFunc={dateTimeLabelFn}
                name="modifiedDate"
                label={t('entities.dWFile.modifiedDate')}
              />
            </Grid>
            {onDelete && (
              <ConfirmationDialogTrigger
                onCloseDialog={this.handleConfirmationDialogAction}
                dialog={{
                  title: t('entities.dWFile.deleteDialog.title'),
                  description: t('entities.dWFile.deleteDialog.description'),
                  confirmLabel: t('common.yes'),
                  discardLabel: t('common.no'),
                }}
                Renderer={({ onClick }) => (
                  <Button onClick={onClick} disabled={isSubmitting}>
                    {t('common.delete')}
                  </Button>
                )}
              />
            )}

            <Button onClick={onCancelEditing} disabled={isSubmitting} data-testid="cancel-btn">
              {t('common.cancel')}
            </Button>

            <Button type="submit" color="primary" disabled={isSubmitting} data-testid="submit-btn">
              {t('common.save')}
            </Button>
          </Grid>
        </form>
      </MuiPickersUtilsProvider>
    );
  }
}

DWFileForm.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.string,
    textField: PropTypes.string,
    submitButton: PropTypes.string,
    button: PropTypes.string,
    downloadAnchor: PropTypes.string,
  }),
  values: formValues,
  touched: formTouched,
  errors: formErrors,
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  onDelete: PropTypes.func,
  onCancelEditing: PropTypes.func,
  isSubmitting: PropTypes.bool.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired,
  i18n: PropTypes.shape({ language: PropTypes.string }).isRequired,
};

DWFileForm.defaultProps = {
  onCancelEditing: () => {},
  classes: {},
  values: {},
  touched: {},
  errors: {},
  onDelete: null,
};

const emptyDWFile = {
  fileId: '',
  fileName: '',
  folderFlag: false,
  size: '',
  downloadUrl: '',
  type: '',
  parentId: '',
  inTrash: false,
  previewLink: '',
  createdDate: null,
  modifiedDate: null,
};

const validationSchema = Yup.object().shape({
  fileId: Yup.string(),
  fileName: Yup.string(),
  folderFlag: Yup.boolean(),
  size: Yup.number(),
  downloadUrl: Yup.string(),
  type: Yup.string(),
  parentId: Yup.string(),
  inTrash: Yup.boolean(),
  previewLink: Yup.string(),
  createdDate: Yup.date().nullable(),
  modifiedDate: Yup.date().nullable(),
});

const formikBag = {
  mapPropsToValues: ({ dWFile }) => dWFile || emptyDWFile,

  enableReinitialize: true,

  validationSchema,

  handleSubmit: (values, { setSubmitting, props: { onSubmit } }) => {
    onSubmit(values);
    setSubmitting(false);
  },

  displayName: 'DWFileForm',
};

export default compose(
  withStyles(styles, { withTheme: true }),
  withTranslation(),
  withFormik(formikBag)
)(DWFileForm);
