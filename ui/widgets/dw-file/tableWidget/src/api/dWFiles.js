import { getFilterQuery } from 'components/filters/utils';
import { getDefaultOptions, request, getUrl } from 'api/helpers';

const resource = 'api/google-drive/files';

const userId = '0001';

const folderId = 'root';

export const apiDWFilesDelete = async (serviceUrl, id) => {
  const url = `${serviceUrl}/${resource}/${id}`;
  const options = {
    ...getDefaultOptions(),
    method: 'DELETE',
  };
  return request(url, options);
};

export const apiDWFilesGet = async (serviceUrl, { filters = [], pagination, mode }) => {
  const filterQuery = getFilterQuery(filters);
  const paginationQuery = pagination
    ? `page=${pagination.page}&size=${pagination.rowsPerPage}`
    : '';

  const url = `${serviceUrl}/${resource}/${userId}/${folderId}`;

  const options = {
    ...getDefaultOptions(),
    method: 'GET',
  };

  return request(url, options);
};
