import { getDefaultOptions, request } from 'api/helpers';

const resource = 'api/google-drive/file';

const userId = '0001';

export const apiDWFileGet = async (serviceUrl, id) => {
  const url = `${serviceUrl}/${resource}/${userId}/${id}`;
  const options = {
    ...getDefaultOptions(),
    method: 'GET',
  };
  return request(url, options);
};

export const apiDWFilePost = async (serviceUrl, dWFile) => {
  const url = `${serviceUrl}/${resource}`;
  const options = {
    ...getDefaultOptions(),
    method: 'POST',
    body: dWFile ? JSON.stringify(dWFile) : null,
  };
  return request(url, options);
};

export const apiDWFilePut = async (serviceUrl, dWFile) => {
  const url = `${serviceUrl}/${resource}`;
  const options = {
    ...getDefaultOptions(),
    method: 'PUT',
    body: dWFile ? JSON.stringify(dWFile) : null,
  };
  return request(url, options);
};

export const apiDWFileDelete = async (serviceUrl, id) => {
  const url = `${serviceUrl}/${resource}/${id}`;
  const options = {
    ...getDefaultOptions(),
    method: 'DELETE',
  };
  return request(url, options);
};
