package com.veriday.filemanagement.controller;

import com.google.api.client.auth.oauth2.Credential;
import com.veriday.filemanagement.google.GoogleAuthHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class GoogleAuthController {

    @RequestMapping(value = "/redirect", method = RequestMethod.GET)
    public ModelAndView redirectToGoogle(HttpSession session) {
        String userId = String.valueOf(session.getAttribute("userId"));

        String userIdKey = userId + "_GoogleAuthHelper";

        GoogleAuthHelper googleAuthHelper = (GoogleAuthHelper) session.getAttribute(userIdKey);

        String redirectUrl = googleAuthHelper.buildLoginUrl();

        return new ModelAndView("redirect:" + redirectUrl);
    }

    @RequestMapping(value = "/oauth2callback", method = RequestMethod.GET)
    public ModelAndView oauth2callback(HttpServletRequest request, HttpSession session) throws Exception {
        String userId = String.valueOf(session.getAttribute("userId"));

        String userIdKey = userId + "_GoogleAuthHelper";

        GoogleAuthHelper googleAuthHelper = (GoogleAuthHelper) session.getAttribute(userIdKey);

        String authCode = request.getParameter("code");

        googleAuthHelper.generateCredential(authCode);

        return new ModelAndView("redirect:/authorize");
    }

    @RequestMapping(value = "/authorize", method = RequestMethod.GET)
    public ModelAndView authorize(HttpServletRequest request, HttpSession session) throws Exception {
        String userId = request.getParameter("userId");

        if (userId == null) {
            userId = String.valueOf(session.getAttribute("userId"));
        }

        String userIdKey = userId + "_GoogleAuthHelper";

        GoogleAuthHelper googleAuthHelper = (GoogleAuthHelper) session.getAttribute(userIdKey);

        if (googleAuthHelper == null) {
            googleAuthHelper = new GoogleAuthHelper(userId);

            session.setAttribute(userIdKey, googleAuthHelper);
            session.setAttribute("userId", userId);
        }

        Credential credential = googleAuthHelper.getGoogleCredential();

        if (credential == null) {
            return new ModelAndView("redirect:/redirect");
        }

        return new ModelAndView("auth");
    }
}
