package com.veriday.filemanagement.repository;

import com.veriday.filemanagement.domain.DWFile;

import java.io.File;
import java.util.List;
import java.util.Optional;

public interface DWFileRepository {

    List<DWFile> listFilesInFolder(String userId, String folderId) throws Exception;

    Optional<DWFile> findById(String userId, String fileId) throws Exception;

    Optional<DWFile> saveFolderInFolder(
        String userId, String parentFolderId, String subFolderName)
        throws Exception;

    Optional<DWFile> uploadFile(
        String userId, File uploadFile, String fileName, String fileType, String folderId)
        throws Exception;

    Optional<DWFile> deleteFile(String userId, String fileId) throws Exception;

}
