import React from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import Box from '@material-ui/core/Box';

import dWFileType from 'components/__types__/dWFile';
import DWFileFieldTable from 'components/dw-file-field-table/DWFileFieldTable';

const DWFileDetails = ({ t, dWFile }) => {
  return (
    <Box>
      <h3 data-testid="details_title">
        {t('common.widgetName', {
          widgetNamePlaceholder: 'DW File',
        })}
      </h3>
      <DWFileFieldTable dWFile={dWFile} />
    </Box>
  );
};

DWFileDetails.propTypes = {
  dWFile: dWFileType,
  t: PropTypes.func.isRequired,
};

DWFileDetails.defaultProps = {
  dWFile: {},
};

export default withTranslation()(DWFileDetails);
