export const INPUT_EVENT_TYPES = {
  tableAdd: 'dWFile.table.add',
  tableSelect: 'dWFile.table.select',
};

export const OUTPUT_EVENT_TYPES = {
  create: 'dWFile.form.create',
  update: 'dWFile.form.update',
  errorCreate: 'dWFile.form.errorCreate',
  errorUpdate: 'dWFile.form.errorUpdate',
};

export const KEYCLOAK_EVENT_TYPE = 'keycloak';
