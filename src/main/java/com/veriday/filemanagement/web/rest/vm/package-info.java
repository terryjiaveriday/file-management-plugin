/**
 * View Models used by Spring MVC REST controllers.
 */
package com.veriday.filemanagement.web.rest.vm;
