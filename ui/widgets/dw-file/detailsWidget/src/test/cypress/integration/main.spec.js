import { customElementName, detailsTitle, entityIdCell } from '../support';

describe('Main', () => {
  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.keycloackLogin(oauth2Data, 'user');
    });
  });

  afterEach(() => {
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.keycloackLogout(oauth2Data);
    });
    cy.clearCache();
  });

  describe('Details widget', () => {
    it('should load the page', () => {
      cy.get(customElementName).should('exist');
    });

    it('should display the right values', () => {
      cy.get(detailsTitle)
        .should('be.visible')
        .should('have.text', "Details about 'DW File'");
      cy.get(entityIdCell).should('not.be.empty');
      cy.contains('entities.dWFile.fileId').should('be.visible');
      cy.contains('entities.dWFile.fileName').should('be.visible');
      cy.contains('entities.dWFile.folderFlag').should('be.visible');
      cy.contains('entities.dWFile.size').should('be.visible');
      cy.contains('entities.dWFile.downloadUrl').should('be.visible');
      cy.contains('entities.dWFile.type').should('be.visible');
      cy.contains('entities.dWFile.parentId').should('be.visible');
      cy.contains('entities.dWFile.inTrash').should('be.visible');
      cy.contains('entities.dWFile.previewLink').should('be.visible');
      cy.contains('entities.dWFile.createdDate').should('be.visible');
      cy.contains('entities.dWFile.modifiedDate').should('be.visible');
    });
  });
});
