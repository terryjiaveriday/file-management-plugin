import { createContext, useState, useEffect, useContext } from 'react';
import get from 'lodash/get';

const AuthContext = createContext(null);
const KEYCLOAK_EVENT_ID = 'keycloak';

const getKeycloakInstance = () => {
  const entando = window.entando || {};
  const initialized = !!entando.keycloak;
  return initialized ? { ...entando.keycloak, initialized} : { initialized };
};

function AuthProvider({ children }) {

  const [keycloak, setKeycloak] = useState({ 
    ...getKeycloakInstance(), 
    initialized: true 
  });

  useEffect(() => {
    const handleEvent = () => {
      setKeycloak({ 
        ...getKeycloakInstance(), 
        initialized: true 
      });
    };

    window.addEventListener(KEYCLOAK_EVENT_ID, handleEvent);
    return () => {
      window.removeEventListener(KEYCLOAK_EVENT_ID, handleEvent);
    }
  }, []);

  return (
    <AuthContext.Provider value={{
      ...keycloak,
      get user() {
        return keycloak ? {
          preferredName: get(keycloak, 'idTokenParsed.preferred_username'),
          email: get(keycloak, 'idTokenParsed.email')
        } 
        : null;
      }
      }}>
      {children}
    </AuthContext.Provider>
  );
}

const useAuth = () => useContext(AuthContext);

export {AuthProvider, useAuth}