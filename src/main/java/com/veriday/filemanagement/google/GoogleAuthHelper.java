package com.veriday.filemanagement.google;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.docs.v1.DocsScopes;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.gmail.GmailScopes;

import java.io.*;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

public final class GoogleAuthHelper {

	public String CALLBACK_URI;
	private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
	private static final String TOKENS_DIRECTORY_PATH = "tokens";
	private static final String CREDENTIALS_FILE_PATH = "/credentials.json";

	private final GoogleAuthorizationCodeFlow flow;

	public GoogleAuthHelper(String userId) throws GeneralSecurityException, IOException {
		InputStream in = GoogleAuthHelper.class.getResourceAsStream(CREDENTIALS_FILE_PATH);

		if (in == null) {
			throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
		}

		NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();

		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		CALLBACK_URI = clientSecrets.getWeb().getRedirectUris().get(0);

		File userTokenPath = new File(TOKENS_DIRECTORY_PATH, userId);

		FileDataStoreFactory fileDataStoreFactory = new FileDataStoreFactory(userTokenPath);

        List<String> scopes = new ArrayList<>();

        scopes.addAll(DriveScopes.all());
        scopes.addAll(GmailScopes.all());
        scopes.addAll(DocsScopes.all());

        flow = new GoogleAuthorizationCodeFlow.Builder(
				httpTransport, JSON_FACTORY, clientSecrets, scopes)
				.setDataStoreFactory(fileDataStoreFactory)
				.setAccessType("offline")
				.build();
	}

    /**
     * Do not need for now
     * @param HTTP_TRANSPORT
     * @param userId
     * @return
     * @throws IOException
     */
    public Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT, String userId) throws IOException {
        InputStream in = GoogleAuthHelper.class.getResourceAsStream(CREDENTIALS_FILE_PATH);

        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }

        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        File userTokenPath = new File(TOKENS_DIRECTORY_PATH, userId);

        FileDataStoreFactory fileDataStoreFactory = new FileDataStoreFactory(userTokenPath);

        List<String> scopes = new ArrayList<>();

        scopes.addAll(DriveScopes.all());
        scopes.addAll(GmailScopes.all());

        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
            HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, scopes)
            .setDataStoreFactory(fileDataStoreFactory)
            .setAccessType("offline")
            .build();

        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();

        AuthorizationCodeInstalledApp authorizationCodeInstalledApp = new AuthorizationCodeInstalledApp(flow, receiver);

        return authorizationCodeInstalledApp.authorize("user");
    }

	public Credential getGoogleCredential() throws Exception {
		Credential credential = flow.loadCredential("user");

		if (credential != null
				&& (credential.getRefreshToken() != null
				|| credential.getExpiresInSeconds() == null
				|| credential.getExpiresInSeconds() > 60)) {
			return credential;
		}

		return null;
	}

	public String buildLoginUrl() {
		final GoogleAuthorizationCodeRequestUrl url = flow.newAuthorizationUrl();

		return url.setRedirectUri(CALLBACK_URI).build();
	}

	public void generateCredential(String authCode) throws Exception {
		final GoogleTokenResponse response = flow.newTokenRequest(authCode).setRedirectUri(CALLBACK_URI).execute();

		flow.createAndStoreCredential(response, "user");
	}

}
