const dWFileMocks = [
  {
    id: 0,
    fileId:
      'Vero minima atque dolor est expedita reiciendis ipsam doloribus. Ea facere tempore voluptatibus placeat quam sed voluptas. Iusto saepe expedita quia accusamus consequuntur magnam aut. Amet et vero consequatur quisquam blanditiis dolorem. Sint maxime doloribus ipsum quam culpa. Occaecati consequatur ut.',
    fileName:
      'Consequatur voluptas a quam dolorum. Animi minima voluptas numquam. Velit sunt unde qui facere voluptatem. Magni perspiciatis ea voluptas et nulla adipisci molestiae accusamus dolor. Sed laborum sunt voluptatum. Iusto quas ducimus nesciunt cum quibusdam earum quidem voluptatem rerum.',
    folderFlag: true,
    size: 897,
    downloadUrl:
      'Voluptatem ea exercitationem eaque quisquam ea. Doloribus a nostrum adipisci dolorem perspiciatis sit cum quia. In et at sed suscipit molestiae molestiae sunt. Nostrum excepturi odit numquam. Molestias quis totam delectus soluta voluptate natus.',
    type:
      'Sint suscipit reiciendis voluptatem voluptas. Qui perferendis ratione quae similique quae laboriosam velit. Perspiciatis deserunt reprehenderit fugiat vel culpa iusto. Corporis animi rerum ut eligendi fugiat accusantium illum quis.',
    parentId:
      'Ipsum est sed inventore perspiciatis eveniet possimus sapiente vel est. Enim magni in dolore est asperiores aut. Ipsa magnam eaque velit sit quos dolor.',
    inTrash: false,
    previewLink:
      'Ipsam est sed. Laudantium non quo ipsam ex commodi. Et ea aut sit est facilis recusandae perspiciatis molestias. Voluptatem praesentium sed maiores nihil eos consequuntur nemo in. Ut cum iure dolorem.',
    createdDate: '1976-12-11T00:49:00-05:00',
    modifiedDate: '2009-02-06T18:17:46-05:00',
  },
  {
    id: 1,
    fileId:
      'Tenetur suscipit modi saepe placeat eaque voluptatem perferendis. Nobis qui tempora enim nesciunt est maiores quidem. Unde facere ut voluptas.',
    fileName:
      'Non quod et libero sapiente quia ut minus atque. Ullam minus quia facilis quo. Beatae vel quisquam repudiandae dicta. Necessitatibus facilis alias aut ullam laudantium aliquid et ut consequuntur. Minima blanditiis quam nulla qui. Error consectetur occaecati aliquid iusto distinctio vel non.',
    folderFlag: true,
    size: -120,
    downloadUrl:
      'Doloribus voluptas quis. Illo nulla ut sunt corporis eaque labore illo. Amet deleniti eos qui sequi. Provident consequuntur dolor quibusdam. Nisi accusamus distinctio at inventore veniam reprehenderit.',
    type:
      'Consequatur distinctio quia voluptate qui in labore aut vero rerum. Corporis minus provident. Soluta dolorem et totam. Architecto unde quia. Quasi rerum quas quibusdam doloribus magnam id qui aliquid commodi.',
    parentId:
      'Exercitationem quidem eos accusamus illo est optio illo consequatur. Libero ea cupiditate cum quibusdam tempora autem ut quaerat nisi. Porro est ad placeat voluptatem perferendis harum recusandae. Et aut sed accusamus. Quia molestias fugit aut quibusdam.',
    inTrash: true,
    previewLink:
      'Aspernatur perspiciatis autem magni. Beatae id sed delectus voluptas. Aut magnam eligendi natus.',
    createdDate: '1995-10-23T03:27:13-04:00',
    modifiedDate: '2007-08-27T19:24:45-04:00',
  },
];

export default dWFileMocks;
