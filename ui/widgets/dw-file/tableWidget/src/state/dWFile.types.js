export const READ_ALL = 'dWFile-table/readAll';
export const ERROR_FETCH = 'dWFile-table/error';
export const CLEAR_ERRORS = 'dWFile-table/clearErrors';
export const CLEAR_ITEMS = 'dWFile-table/clearItems';
export const CREATE = 'dWFile-table/create';
export const UPDATE = 'dWFile-table/update';
export const DELETE = 'dWFile-table/delete';
